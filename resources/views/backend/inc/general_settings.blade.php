<section class="page-header mb-3">
    <div class="container-fluid">
        <h1><i class="icon-cog"></i> General Settings</h1>
        <ul class="page-breadcrumb">
            <li><a href="{{ url($company) }}"><i class="icon-dashboard"></i> Dashboard</a></li>
            <li class="active">General Settings</li>
        </ul>
    </div>
</section>
<div class="container-fluid">
    {{ Form::open(['files' => true]) }}
        <div class="row">
            <div class="col-sm-8 col-lg-9">
                <div class="card">
                    <h3 class="card-title mb-3">
                        Company Information
                    </h3>
                    @if (\Session::has('success'))
                    <div class="alert alert-success">
                        {!! \Session::get('success') !!}
                    </div>
                    @endif

                    <div class="form-group">
                        <label>Site URL</label>
                        <input type="text" value="{{ url($profile->company->company_sitename) }}" class="form-control" readonly>
                    </div>
                    <div class="form-group">
                        <label>Company Name (Required)</label>
                        <input type="text" name="compnay[company_name]" value="{{ @$profile->company->company_name }}" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Company Tagline</label>
                        <input type="text" name="compnay[company_tagline]" value="{{ @$profile->company->company_tagline }}" class="form-control">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-lg-3">
                <div class="card">
                    <h3 class="card-title">Upload Logo</h3>
                    <label class="upload_image">
                        <img src="{{ empty($profile->company->company_logo) ? url('imgs/no-image.png') : url("imgs/companies/{$profile->company->company_logo}") }}" alt="Upload Image" title="Upload Image" style="width: 256px; height: 256px; object-fit: contain;">
                        <input type="file" name="company_logo" accept="image/*" id="userImage">
                    </label>
                    <label for="userImage" class="btn btn-primary btn-block">Choose Image</label>
                </div>

                <div class="card">
                    <h3 class="card-title">Upload Favicon</h3>
                    <label class="upload_image">
                        <img src="{{ empty($profile->company->company_favicon) ? url('imgs/no-image.png') : url("imgs/companies/{$profile->company->company_favicon}") }}" alt="Upload Image" title="Upload Image" style="width: 128px; height: 128px; object-fit: contain;">
                        <input type="file" name="company_favicon" accept="image/*" id="userImage">
                    </label>
                    <label for="userImage" class="btn btn-primary btn-block">Choose Image</label>
                </div>
            </div>
        </div>
    {{ Form::close() }}
</div>
