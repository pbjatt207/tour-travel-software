<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1><i class="icon-user2"></i> {{ sprintf( '#%06d', $lead->lead_id ) }}</h1>
                <ul class="page-breadcrumb">
                    <li><a href="{{ url($company) }}"><i class="icon-dashboard"></i> Dashboard</a></li>
                    <li class="active">Leads</li>
                </ul>
            </div>
            <div class="float-right">
                <a href="{{ url($company.'/leads/add') }}" class="btn btn-primary">Create New</a>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    @if (\Session::has('success'))
    <div class="alert alert-success">
        {!! \Session::get('success') !!}
    </div>
    @endif

    <div class="row">
        <div class="col-sm-6">
            <div class="card">
                <h3 class="card-title"><i class="icon-user"></i> Customer Details</h3>

                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tr>
                            <th>Name</th>
                            <td>{{ $lead->user->user_name }}</td>
                        </tr>
                        <tr>
                            <th>Contact No.</th>
                            <td>{{ $lead->user->user_mobile }}</td>
                        </tr>
                        <tr>
                            <th>Email ID</th>
                            <td>{{ $lead->user->user_email }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="card">
                <h3 class="card-title"><i class="icon-question1"></i> Lead Info</h3>

                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tr>
                            <th>Lead Tracking ID</th>
                            <td>{{ sprintf( '#%06d', $lead->lead_id ) }}</td>
                            <th>Enquiry For</th>
                            <td>{{ $lead->lead_query_for }}</td>
                            @if($lead->lead_query_for == "Visa")
                            <th>Travel Country</th>
                            <td>{{ $lead->country->country_name }} ({{ $lead->country->country_short_name }})</td>
                            @endif
                        </tr>
                        @if($lead->lead_query_for == "Visa")
                        <tr>
                            <th>No. of Adult</th>
                            <td>{{ $lead->lead_adult_no }}</td>
                            <th>No. of Kids</th>
                            <td>{{ $lead->lead_kid_no }}</td>
                            <th>No. of Infant</th>
                            <td>{{ $lead->lead_infant }}</td>
                        </tr>
                        @endif
                        <tr>
                            <th colspan="1">Remarks</th>
                            <td colspan="5">{!! $lead->lead_remark !!}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

	<div class="card">
        <h3 class="card-title">Follow-ups</h3>
        @if(!$records->isEmpty())
            {{ Form::open() }}
		    <div class="table-responsive">
			    <table class="table table-bordered">
			          <thead>
			               <tr>
			                   <th style="width: 50px;">S.No.</th>
                               <th>Message</th>
                               <th>Date</th>
                               <th>Next Follow-up Date</th>
                               <th>Given By</th>
			               </tr>
			          </thead>

			          <tbody>
			          		@php $sn = $records->firstItem(); @endphp
			          		@foreach($records as $rec)
			               	<tr>
								<td>{{ $sn++ }}</td>
                                <td>{{ $rec->lfu_message }}</td>
                                <td>{!! strtotime($rec->lfu_date) > 0 ? date( 'd/m/Y', strtotime($rec->lfu_date) ) : '&mdash;' !!}</td>
                                <td>{!! strtotime($rec->lfu_next_date) > 0 ? date( 'd/m/Y', strtotime($rec->lfu_next_date) ) : '&mdash;' !!}</td>
                                <td>{{ $rec->support->user_name }}</td>
			               	</tr>
			               @endforeach
			          </tbody>
			    </table>
			</div>
            @php
                $get_param = request()->input();
                if(isset($get_param['page'])) {
                    unset($get_param['page']);
                }
            @endphp
            {{ $records->appends($get_param)->links() }}
		    @else
		    <div class="no_records_found">
		      No records found yet.
		    </div>
			@endif
		{{ Form::close() }}
	</div>

    <div class="card">
        <h3 class="card-title">Follow-up Message</h3>
        <input type="hidden" name="record[lfu_date]" value="">
        <div class="form-group">
            <label>Next Follow-up Date</label>
            <input type="text" name="record[lfu_next_date]" value="" class="form-control datepicker_no_past" placeholder="yyyy-mm-dd" readonly required>
        </div>
        <div class="form-group">
            <label>Message</label>
            <textarea name="record[lfu_message]" rows="5" class="form-control" placeholder="Write some message" required></textarea>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit <i class="icon-long-arrow-right"></i> </button>
        </div>
    </div>
</div>
