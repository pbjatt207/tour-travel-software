<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1><i class="icon-globe"></i> States</h1>
                <ul class="page-breadcrumb">
                    <li><a href="{{ url($company_info->company_sitename) }}"><i class="icon-dashboard"></i> Dashboard</a></li>
                    <li class="active">States</li>
                </ul>
            </div>
            <div class="float-right">
                {{ Form::open(['url' => url( $company_info->company_sitename.'/location/state/import'), 'files' => true]) }}
                <label class="btn btn-primary import_csv">
                    Import CSV
                    <input name="import_file" type="file" accept=".csv">
                </label>
                <a href="{{ url($company_info->company_sitename.'/location/state/export') }}" class="btn btn-primary">Export CSV</a>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    @if (\Session::has('success'))
    <div class="alert alert-success">
        {!! \Session::get('success') !!}
    </div>
    @endif
    <div class="row">
        <div class="col-sm-4">
        	<div class="card">
                @if (\Session::has('danger'))
                <div class="alert alert-danger">
                    {!! \Session::get('danger') !!}
                </div>
                @endif
                {{ Form::open() }}
                    <h3 class="card-title"><i class="icon-globe"></i> {{ !empty($edit->state_id) ? "Edit" : "Add" }} State</h3>
                    <div class="form-group">
                        <label>Country (Required)</label>
                        <select name="record[state_country]" class="form-control" required>
                            <option value="">Select Country</option>
                            @foreach($countries as $con)
                                <option value="{{ $con->country_id }}" @if(!empty($edit->state_country) && $edit->state_country == $con->country_id) selected @endif>{{ $con->country_name.' ('.$con->country_short_name.')' }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>State Name (Required)</label>
                        <input type="text" name="record[state_name]" value="{{ @$edit->state_name }}" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>State Short Name (Required)</label>
                        <input type="text" name="record[state_short_name]" value="{{ @$edit->state_short_name }}" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-block btn-primary">Submit</button>
                    </div>
                {{ Form::close() }}
        	</div>
        </div>
        <div class="col-sm-8">
            <div class="card">
                <h3 class="card-title"><i class="icon-filter1"></i> Filter By</h3>
                {{ Form::open(['method' => 'GET']) }}
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <select name="search[country]" class="form-control">
                                <option value="">Select Country</option>
                                @foreach($countries as $con)
                                    <option value="{{ $con->country_id }}" @if(@$search['country'] == $con->country_id) selected @endif>{{ $con->country_name.' ('.$con->country_short_name.')' }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <input type="text" name="search[keyword]" value="{{ @$search['keyword'] }}" class="form-control" placeholder="By keywords">
                        </div>
                    </div>
                    <div class="col-sm-3 col-xl-2">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block"><i class="icon-search"></i> Search</button>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        	<div class="card">
        		<form method="post">
                    <a href="#remove" class="float-right" data-toggle="tooltip" title="Remove Selected"> <i class="icon-trash-o"></i> </a>
            		<h3 class="card-title">
                        <div class="mr-auto"><i class="icon-globe"></i> View States</div>
            		</h3>
        	    	@csrf
        		    @if(!$records->isEmpty())
        		    <div class="table-responsive">
        			    <table class="table table-bordered">
        			          <thead>
        			               <tr>
        			                    <th style="width: 50px;">
        			                        <label class="animated-checkbox">
        			                            <input type="checkbox" class="checkall">
        			                            <span class="label-text"></span>
        			                        </label>
        			                    </th>
        			                   <th style="width: 50px;">S.No.</th>
                                       <th>State Name</th>
                                       <th>State Short Name</th>
        			                   <th>Country</th>
        			               </tr>
        			          </thead>

        			          <tbody>
        			          		@php $sn = $records->firstItem(); @endphp
        			          		@foreach($records as $rec)
        			               	<tr>
        		                        <td>
        		                            <label class="animated-checkbox">
        		                                <input type="checkbox" name="check[]" value="{{ $rec->state_id  }}" class="check @if($rec->cities_count) disabled @endif" @if($rec->cities_count) disabled @endif>
        		                                <span class="label-text"></span>
        		                            </label>
        		                        </td>
        								<td>{{ $sn++ }}</td>
                                        <td>
                                            <a href="{{ url($company_info->company_sitename.'/location/states/'.$rec->state_id) }}" class="pencil">
                                                <i class="icon-pencil" title="Edit"></i> {{ $rec->state_name }}
                                            </a>
                                            ({{ $rec->cities_count }} Cities)
                                        </td>
                                        <td>{{ $rec->state_short_name }}</td>
        								<td>{{ $rec->country->country_name }}</td>
        			               	</tr>
        			               @endforeach
        			          </tbody>
        			    </table>
        			</div>
                    @php
                        $get_param = request()->input();
                        if(isset($get_param['page'])) {
                            unset($get_param['page']);
                        }
                    @endphp
                    {{ $records->appends($get_param)->links() }}
        		    @else
        		    <div class="no_records_found">
        		      No records found yet.
        		    </div>
        			@endif
        		</form>
        	</div>
        </div>
    </div>
</div>
