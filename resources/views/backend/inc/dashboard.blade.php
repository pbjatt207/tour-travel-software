<div class="page-header">
	<div class="container-fluid">
		<h1><i class="icon-dashboard"></i> Dashboard</h1>
		<p>The summary of all modules</p>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-3">
			<div class="card bg-primary text-white">
				<h3 class="card-title"><i class="icon-info"></i> Total Enquiries</h3>
				<div class="dash-number">
					158
				</div>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="card bg-info text-white">
				<h3 class="card-title"><i class="icon-info"></i> Active Enquiries</h3>
				<div class="dash-number">
					50
				</div>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="card bg-success text-white">
				<h3 class="card-title"><i class="icon-info"></i> Converted Enquiries</h3>
				<div class="dash-number">
					100
				</div>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="card bg-danger text-white">
				<h3 class="card-title"><i class="icon-info"></i> Dropped Enquiries</h3>
				<div class="dash-number">
					8
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="card" style="padding: 0;">
				<div class="card-header bg-success text-white"><i class="icon-tag"></i> Sale Summary</div>
				<div class="card-body">
					<div class="text-center two_cols">
						<div>
							<h4>₹3,505.00</h4>
							<div class="text-primary">
								Sale
							</div>
						</div>
						<div>
							<h4>₹1,505.00</h4>
							<div class="text-success">
								Received
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="card" style="padding: 0;">
				<div class="card-header bg-info text-white"><i class="icon-tag"></i> Purchase Summary</div>
				<div class="card-body">
					<div class="text-center two_cols">
						<div>
							<h4>₹200.00</h4>
							<div class="text-primary">
								Purchase
							</div>
						</div>
						<div>
							<h4>₹200.00</h4>
							<div class="text-success">
								Paid
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="text-center">
		<div class="btn-group nav d-inline-flex">
		  <button type="button" class="btn btn-outline-info active" data-toggle="tab" data-target="#ongoingTours">Ongoing Tours</button>
		  <button type="button" class="btn btn-outline-info" data-toggle="tab" data-target="#upcomingTours">Upcoming Tours</button>
		  <button type="button" class="btn btn-outline-info" data-toggle="tab" data-target="#packageTours">Package Tours</button>
		  <button type="button" class="btn btn-outline-info" data-toggle="tab" data-target="#groupTours">Group Tours</button>
		  <button type="button" class="btn btn-outline-info" data-toggle="tab" data-target="#followups">Followups</button>
		</div>
	</div>

	<!-- Tab panes -->
	<div class="tab-content mt-3">
		<div class="tab-pane active" id="ongoingTours">
			<div class="card">
				<h3>Ongoing Tours</h3>
				<div class="table-responsive">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>S.No.</th>
								<th>Enquiry ID</th>
								<th>Customer Name</th>
								<th>Tour</th>
								<th>Mobile</th>
								<th>Followups Date Time</th>
								<th>Assigned To</th>
							</tr>
						</thead>
						<tbody>
							@for($i = 1; $i <= 5; $i++)
							<tr>
								<td>{{ $i }}</td>
								<th>{{ sprintf("%05d", $i) }}</th>
								<th>Name {{ $i }}</th>
								<th>Car Rental</th>
								<th>9988774455</th>
								<th>{{ date('d M Y h:i A', time()) }}</th>
								<th>N/A</th>
							</tr>
							@endfor
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="upcomingTours">
			<div class="card">
				<h3>Upcoming Tours</h3>
				<div class="table-responsive">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>S.No.</th>
								<th>Enquiry ID</th>
								<th>Customer Name</th>
								<th>Tour</th>
								<th>Mobile</th>
								<th>Followups Date Time</th>
								<th>Assigned To</th>
							</tr>
						</thead>
						<tbody>
							@for($i = 1; $i <= 5; $i++)
							<tr>
								<td>{{ $i }}</td>
								<th>{{ sprintf("%05d", $i) }}</th>
								<th>Name {{ $i }}</th>
								<th>Car Rental</th>
								<th>9988774455</th>
								<th>{{ date('d M Y h:i A', time()) }}</th>
								<th>N/A</th>
							</tr>
							@endfor
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="packageTours">
			<div class="card">
				<h3>Package Tours</h3>
				<div class="table-responsive">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>S.No.</th>
								<th>Enquiry ID</th>
								<th>Customer Name</th>
								<th>Tour</th>
								<th>Mobile</th>
								<th>Followups Date Time</th>
								<th>Assigned To</th>
							</tr>
						</thead>
						<tbody>
							@for($i = 1; $i <= 5; $i++)
							<tr>
								<td>{{ $i }}</td>
								<th>{{ sprintf("%05d", $i) }}</th>
								<th>Name {{ $i }}</th>
								<th>Car Rental</th>
								<th>9988774455</th>
								<th>{{ date('d M Y h:i A', time()) }}</th>
								<th>N/A</th>
							</tr>
							@endfor
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="groupTours">
			<div class="card">
				<h3>Group Tours</h3>
				<div class="table-responsive">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>S.No.</th>
								<th>Enquiry ID</th>
								<th>Customer Name</th>
								<th>Tour</th>
								<th>Mobile</th>
								<th>Followups Date Time</th>
								<th>Assigned To</th>
							</tr>
						</thead>
						<tbody>
							@for($i = 1; $i <= 5; $i++)
							<tr>
								<td>{{ $i }}</td>
								<th>{{ sprintf("%05d", $i) }}</th>
								<th>Name {{ $i }}</th>
								<th>Car Rental</th>
								<th>9988774455</th>
								<th>{{ date('d M Y h:i A', time()) }}</th>
								<th>N/A</th>
							</tr>
							@endfor
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="followups">
			<div class="card">
				<h3>Followups</h3>
				<div class="table-responsive">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>S.No.</th>
								<th>Enquiry ID</th>
								<th>Customer Name</th>
								<th>Tour</th>
								<th>Mobile</th>
								<th>Followups Date Time</th>
								<th>Assigned To</th>
							</tr>
						</thead>
						<tbody>
							@for($i = 1; $i <= 5; $i++)
							<tr>
								<td>{{ $i }}</td>
								<th>{{ sprintf("%05d", $i) }}</th>
								<th>Name {{ $i }}</th>
								<th>Car Rental</th>
								<th>9988774455</th>
								<th>{{ date('d M Y h:i A', time()) }}</th>
								<th>N/A</th>
							</tr>
							@endfor
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
