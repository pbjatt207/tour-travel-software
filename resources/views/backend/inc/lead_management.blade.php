<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1><i class="icon-clock"></i> Lead Management</h1>
                <ul class="page-breadcrumb">
                    <li><a href="{{ url($company) }}"><i class="icon-dashboard"></i> Dashboard</a></li>
                    <li class="active">Lead Management</li>
                </ul>
            </div>
            <div class="float-right">
                <a href="{{ url($company.'/leads/add') }}" class="btn btn-primary">Create New</a>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    @if (\Session::has('success'))
    <div class="alert alert-success">
        {!! \Session::get('success') !!}
    </div>
    @endif
    <div class="card">
        <h3 class="card-title"><i class="icon-filter1"></i> Filter By</h3>
        {{ Form::open(['method' => 'GET']) }}
        <div class="row">
            <div class="col-sm-2">
                <div class="form-group">
                    <input type="text" name="search[keyword]" value="{{ @$search['keyword'] }}" class="form-control" placeholder="By keywords">
                </div>
            </div>

            <div class="col-sm-2">
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block"><i class="icon-search"></i> Search</button>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
	<div class="card">
        {{ Form::open() }}
        <select class="float-right" name="lead_staff" id="select_staff">
            <option value="">Select Staff</option>
            @foreach($staffs as $s)
            <option value="{{ $s->user_id }}">{{ $s->user_name }}</option>
            @endforeach
        </select>
        <p>
            <a href="{{ url($company.'/leads/management') }}">All</a> | <a href="{{ url($company.'/leads/management/?assigned=no') }}">Not Assigned</a> | <a href="{{ url($company.'/leads/management/?assigned=yes') }}">Assigned</a>
        </p>
        @if(!$records->isEmpty())
            <!-- <a href="#remove" class="float-right" data-toggle="tooltip" title="Remove Selected"> <i class="icon-trash-o"></i> </a> -->
    		<!-- <h3 class="card-title">
                {{ $records->total() }} records(s) found.
    		</h3> -->

		    <div class="table-responsive">
			    <table class="table table-bordered">
			          <thead>
			               <tr>
			                    <th style="width: 50px;">
			                        <label class="animated-checkbox">
			                            <input type="checkbox" class="checkall">
			                            <span class="label-text"></span>
			                        </label>
			                    </th>
			                   <th style="width: 50px;">S.No.</th>
                               <th>Lead Track ID</th>
                               <th>Lead For</th>
                               <th>Remarks</th>
                               <th>Assigned Staff</th>
			               </tr>
			          </thead>

			          <tbody>
			          		@php $sn = $records->firstItem(); @endphp
			          		@foreach($records as $rec)
                            @php
                                $image   = !empty( $rec->user->user_image ) ? url( "/imgs/users/{$company}/".$rec->user->user_image ) : url( 'imgs/no-image.png' );
                            @endphp
			               	<tr>
		                        <td>
		                            <label class="animated-checkbox">
		                                <input type="checkbox" name="check[]" value="{{ $rec->lead_id  }}" class="check">
		                                <span class="label-text"></span>
		                            </label>
		                        </td>
								<td>{{ $sn++ }}</td>
                                <td>
                                    <i class="icon-pencil" title="Edit"></i> {{ sprintf("#%06d", $rec->lead_id)  }}
                                </td>
                                <td>
                                    {{ $rec->lead_query_for }}
                                </td>
								<td>{{ $rec->lead_remark }}</td>
                                <td>{{ !empty($rec->staff->user_name) ? $rec->staff->user_name : "-" }}</td>
			               	</tr>
			               @endforeach
			          </tbody>
			    </table>
			</div>
            @php
                $get_param = request()->input();
                if(isset($get_param['page'])) {
                    unset($get_param['page']);
                }
            @endphp
            {{ $records->appends($get_param)->links() }}
		    @else
		    <div class="no_records_found">
		      No records found yet.
		    </div>
			@endif
		{{ Form::close() }}
	</div>
</div>
