<section class="page-header mb-3">
    <div class="container-fluid">
        <div >
            <div>
                <h1><i class="icon-building"></i> Plan Your Trip</h1>
                <ul class="page-breadcrumb">
                    <li><a href="{{ url($company) }}"><i class="icon-dashboard"></i> Dashboard</a></li>
                    <li class="active">Plan Your Trip</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    @if (\Session::has('success'))
    <div class="alert alert-success">
        {!! \Session::get('success') !!}
    </div>
    @endif
    <div class="row">
        <div class="col-sm-8">
        	<div class="card fancy_form">
                {{ Form::open(['autocomplete' => 'off', 'id' => 'travelForm']) }}
                <div class="form-group">
                    <label>Travel Date</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-calendar"></i></span>
                        </div>
                        <input type="text" id="travel_date" value="{{ date('d M Y') }}" class="form-control tavel_date" readonly>
                    </div>
                    <input type="hidden" name="record[travel_date]" id="alt_travel_date" value="{{ date('Y-m-d') }}">
                    <p class="text-primary mt-3"><strong>Note:</strong> Pickup & Drop point are required, if you want vehicle/itinerary.</p>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Pick-Up Point</label>
                                <div class="input-group autocomplete">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="icon-map-marker"></i></span>
                                    </div>
                                    <input type="text" name="record[pickup_point]" value="" class="form-control" id="pickupPoint" placeholder="Pick Up location?" autocomplete="off" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Drop Point</label>
                                <div class="input-group autocomplete">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="icon-map-marker"></i></span>
                                    </div>
                                    <input type="text" name="record[drop_point]" value="" class="form-control" id="dropPoint" placeholder="Drop location?" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>No. of Adult</label>
                                <div class="number-input">
                                    <input type="number" name="record[no_of_adult]" value="1" min="1" class="form-control">
                                    <div class="num-input-change">
                                        <span class="icon-plus2 plus"></span>
                                        <span class="icon-minus2 minus"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>No. of Child</label>
                                <div class="number-input">
                                    <input type="number" name="record[no_of_child]" value="0" min="0" class="form-control child_no">
                                    <div class="num-input-change">
                                        <span class="icon-plus2 plus child_plus"></span>
                                        <span class="icon-minus2 minus child_minus"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>No. of Infant</label>
                                <div class="number-input">
                                    <input type="number" name="record[no_of_infant]" value="0" min="0" class="form-control">
                                    <div class="num-input-change">
                                        <span class="icon-plus2 plus"></span>
                                        <span class="icon-minus2 minus"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="age_child">
                      <div class="row">

                      </div>
                    </div>
                    <div id="buildtour">
                       <ul class="sortable list-unstyled ui-sortable"> </ul>
                    </div>
                    <div class="form-group">
                        <label>Visit Place</label>
                        <div class="input-group autocomplete">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-map-marker"></i></span>
                            </div>
                            <input type="text" value="" id="visitPlace" placeholder="Where you wanna go?" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">SEARCH</button>
                    </div>
                    <input type="hidden" id="total_place" value="0">
                </div>
        		{{ Form::close() }}
        	</div>
        </div>
    </div>

</div>
