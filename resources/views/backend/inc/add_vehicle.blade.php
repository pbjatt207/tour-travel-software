<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1><i class="icon-tasks"></i> {{ !empty($edit->lead_id) ? "Edit" : "Add" }} Vehicle</h1>
                <ul class="page-breadcrumb">
                    <li><a href="{{ url($company) }}"><i class="icon-dashboard"></i> Dashboard</a></li>
                    <li><a href="{{ url($company.'/vehicles') }}">Vehicle</a></li>
                    <li class="active">{{ !empty($edit->vehicle_id) ? "Edit" : "Add" }} Vehicle</li>
                </ul>
            </div>
            <div class="float-right">
                <a href="{{ url($company.'/vehicle/') }}" class="btn btn-primary">View Vehicles</a>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    {{ Form::open(["files" => true]) }}
        @if (\Session::has('danger'))
            <div class="alert alert-danger">
                {!! \Session::get('danger') !!}</li>
            </div>
        @endif
    	<div class="row">
    		<div class="col-sm-8 col-lg-9">
                <div class="card">
                    <h3 class="card-title">Vehicle Information</h3>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Vehicle Make (Required)</label>
                                <input type="text" name="record[vehicle_make]" value="{{ @$edit->vehicle_make }}" placeholder="Vehicle Make" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Vehicle Model (Required)</label>
                                <input type="text" name="record[vehicle_model]" value="{{ @$edit->vehicle_model }}" placeholder="Vehicle Model" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Vehicle Seats with Driver (Required)</label>
                                <input type="tel" name="record[vehicle_seat]" value="{{ @$edit->vehicle_seat }}" placeholder="Vehicle Seat" class="form-control" required>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-lg-3">
                <div class="card">
                    <button type="submit" class="btn btn-block btn-primary">Submit</button>
                </div>
            </div>
        {{ Form::close() }}
    </div>
</div>
