<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1><i class="icon-user2"></i> {{ $type }} Follow-up</h1>
                <ul class="page-breadcrumb">
                    <li><a href="{{ url($company) }}"><i class="icon-dashboard"></i> Dashboard</a></li>
                    <li class="active">Leads</li>
                </ul>
            </div>
            <div class="float-right">
                <a href="{{ url($company.'/leads/add') }}" class="btn btn-primary">Create New</a>
            </div>
        </div>
    </div>
</section>
<div id="lead-follow-popup" class="popup-container">
    <div class="popup-bg"> </div>
    <div class="popup-body" style="width: 900px;">
        <a href="#lead-follow-popup" data-toggle="popup-open" class="close-icon">&times;</a>
        <h3 class="popup-heading">Lead Follow</h3>
        <div class="follow-response">
            {{ Form::open(['url' => url($company.'/ajax/add_customer'), 'id' => 'addCustomerForm', 'data-target' => '#customer-select']) }}
            <div class="form-msg"> </div>

            <div class="form-group text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
<div class="container-fluid">
    @if (\Session::has('success'))
    <div class="alert alert-success">
        {!! \Session::get('success') !!}
    </div>
    @endif
    <div class="card">
        <h3 class="card-title"><i class="icon-filter1"></i> Filter By</h3>
        {{ Form::open(['method' => 'GET']) }}
        <div class="row">
            <div class="col-sm-2">
                <div class="form-group">
                    <label for="">By Lead ID or Message</label>
                    <input type="text" name="search[keyword]" value="{{ @$search['keyword'] }}" class="form-control" placeholder="By Lead ID or Message">
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label for="">Lead For</label>
                    <select class="form-control" name="search[enq_for]">
                        <option value="">All</option>
                        <option value="FIT" @if(@$search['enq_for'] == "Visa") selected @endif>FIT (Free Independent Traveler)</option>
                        <option value="Visa" @if(@$search['enq_for'] == "Visa") selected @endif>Visa</option>
                        <option value="Hotel" @if(@$search['enq_for'] == "Hotel") selected @endif>Hotel</option>
                        <option value="Air ticket" @if(@$search['enq_for'] == "Air ticket") selected @endif>Air ticket</option>
                        <option value="Holidays Packages" @if(@$search['enq_for'] == "Holidays Packages") selected @endif>Holidays Packages</option>
                    </select>
                </div>
            </div>

            <div class="col-sm-2">
                <div class="form-group">
                    <label for="">&nbsp;</label>
                    <button type="submit" class="btn btn-primary btn-block"><i class="icon-search"></i> Search</button>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
	<div class="card">
        <p>
            <a href="{{ url($company.'/leads/follow-up/today') }}">Today</a> | <a href="{{ url($company.'/leads/follow-up/all') }}">All</a>
        </p>
        @if(!$records->isEmpty())
            {{ Form::open() }}
		    <div class="table-responsive">
			    <table class="table table-bordered">
			          <thead>
			               <tr>
			                   <th style="width: 50px;">S.No.</th>
                               <th>Lead Track ID</th>
                               <th>Name</th>
                               <th>Mobile No.</th>
                               <th>Lead For</th>
                               <th>Last Follow-up Date</th>
                               <th>Follow-up Date</th>
                               <th>Last Message</th>
                               <th>Contact Mode</th>
                               <th>Action</th>
			               </tr>
			          </thead>

			          <tbody>
			          		@php $sn = $records->firstItem(); @endphp
			          		@foreach($records as $rec)
                            @php
                                $image   = !empty( $rec->user->user_image ) ? url( "/imgs/users/{$company}/".$rec->user->user_image ) : url( 'imgs/no-image.png' );
                            @endphp
			               	<tr>
								<td>{{ $sn++ }}</td>
                                <td>
                                    <i class="icon-pencil" title="Edit"></i> {{ sprintf("#%06d", $rec->lead_id)  }}
                                </td>
                                <td>{{ @$rec->lead_customer_name }}</td>
                                <td>{{ @$rec->lead_contact_no }}</td>
                                <td>{{ @$rec->lead_query_for }}</td>
                                <td>{!! strtotime($rec->lfu_date) > 0 ? date( 'd/m/Y', strtotime($rec->lfu_date) ) : '&mdash;' !!}</td>
                                <td>{!! strtotime($rec->lfu_next_date) > 0 ? date( 'd/m/Y', strtotime($rec->lfu_next_date) ) : '&mdash;' !!}</td>
                                <td>{{ $rec->lfu_message }}</td>
								<td>{{ $rec->lfu_contact_mode }}</td>
                                <td> <button type="button" data-id="{{ $rec->lead_id }}" class="btn btn-primary btn-sm follow-popup-btn">Follow-up</button> </td>
			               	</tr>
			               @endforeach
			          </tbody>
			    </table>
			</div>
            @php
                $get_param = request()->input();
                if(isset($get_param['page'])) {
                    unset($get_param['page']);
                }
            @endphp
            {{ $records->appends($get_param)->links() }}
		    @else
		    <div class="no_records_found">
		      No records found yet.
		    </div>
			@endif
		{{ Form::close() }}
	</div>
</div>
