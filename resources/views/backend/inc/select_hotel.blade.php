<section class="page-header mb-3">
    <div class="container-fluid">
        <div >
            <div>
                <h1><i class="icon-hotel"></i> Choose Your Stay</h1>
                <ul class="page-breadcrumb">
                    <li><a href="{{ url($company) }}"><i class="icon-dashboard"></i> Dashboard</a></li>
                    <li class="active">Choose Your Stay</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    @if (\Session::has('success'))
    <div class="alert alert-success">
        {!! \Session::get('success') !!}
    </div>
    @endif
    <div class="row">
        <div class="col-sm-3 col-lg-2">
            <ul class="vertical-tabs">
                @php
                    $sn = 0;
                @endphp
                @foreach(@$travel_session['visit_place'] as $place)
                    @php
                        $sn++;
                    @endphp
                    <li @if($sn == 1) class="active" @endif><a href="#place_{{ $sn }}">{{ $place }}</a></li>
                @endforeach
            </ul>
        </div>
        <div class="col-sm-9 col-lg-10">
            @php
                $sn = 0;
            @endphp
            @foreach(@$travel_session['visit_place'] as $place)
            @php
                $sn++;
            @endphp
        	<div class="card hotel-place @if($sn == 1) active @endif" id="place_{{ $sn }}">
                <div class="d-flex align-middle mb-4">
                    <div>Own Stay</div>
                    <div class="press">
                        <input type="checkbox" id="unchecked_{{ $sn }}" class="cbx hidden"/>
                        <label for="unchecked_{{ $sn }}" class="lbl"></label>
                    </div>
                </div>
                <div class="row">
                  @if(!empty($hotelArr[$place]->Hotels->Hotel))
                    @foreach($hotelArr[$place]->Hotels->Hotel as $hotel)
                    <div class="col-sm-3">
                        <div class="hotel-box">
                            <div class="">
                                <img src="{{ $hotel->ThumbImages }}" alt="">
                            </div>
                            <div class="hotel-content">
                                <div class="hotel-name">
                                    {{ $hotel->Name }}
                                </div>
                                <!-- <p>Pal by pass road</p> -->
                                <div class="row">
                                    <div class="col-6">
                                        <a href="#room_data" data-toggle="modal" class="btn btn-sm btn-block btn-primary">Select</a>
                                    </div>
                                    <div class="col-6 text-right">
                                        <i class="icon-info"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade"  id="login_modal">
                              <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h2 class="modal-title">Login</h2>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>

                                  <div class="modal-body">
                                      {!! Form::open(['url'=>'ajax/user_login','id'=>'user_login', 'class'=>'login-form-modal', 'method' => 'POST']) !!}
                                      {!! Form::hidden('user_id','',array('id'=>'user-id')) !!}
                                            <input type="hidden" name="record[user_role]" value="customer">
                                            <div class="floating-label">
                                              <input class="floating-input form-control" type="text" placeholder=" " name="record[user_login]" required>
                                              <span class="highlight"></span>
                                              <label>User Name</label>
                                            </div>
                                            <div class="floating-label">
                                              <input class="floating-input form-control" type="password" placeholder=" "  name="record[user_password]" required>
                                              <span class="highlight"></span>
                                              <label>Password</label>
                                            </div>
                                            <div class="form-msg"></div>
                                        {!! Form::submit('Login Now',['class' => 'btn login-btn']) !!}
                                      {!! Form::close() !!}
                                      <div class="forgot-password-outer">
                                        <a data-toggle="tab" href="#forget-content" aria-expanded="true">Forgot Password</a>
                                      </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                        </div>
                    </div>
                    @endforeach
                  @endif
                </div>
        	</div>
            @endforeach
        </div>
    </div>

</div>
