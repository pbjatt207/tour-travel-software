<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1><i class="icon-user2"></i> {{ !empty($edit->user_id) ? "Edit" : "Add" }} {{ $role->role_name }} Account</h1>
                <ul class="page-breadcrumb">
                    <li><a href="{{ url($company) }}"><i class="icon-dashboard"></i> Dashboard</a></li>
                    <li><a href="{{ url($company.'/account/'.$role->role_slug) }}">{{ $role->role_name }} Account</a></li>
                    <li class="active">{{ !empty($edit->user_id) ? "Edit" : "Add" }} {{ $role->role_name }} Account</li>
                </ul>
            </div>
            <div class="float-right">
                <a href="{{ url($company.'/account/'.$role->role_slug) }}" class="btn btn-primary">View {{ $role->role_name }} Account</a>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    {{ Form::open(["files" => true]) }}
        @if (\Session::has('danger'))
            <div class="alert alert-danger">
                {!! \Session::get('danger') !!}</li>
            </div>
        @endif
    	<div class="row">
    		<div class="col-sm-8 col-lg-9">
                <div class="card">
                    <h3 class="card-title">Login Details</h3>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>User ID (Required)</label>
                                <input type="text" name="user[user_login]" value="{{ @$edit->user_login }}" class="form-control" placeholder="User ID" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Password (Required)</label>
                                <input type="password" name="user[user_password]" class="form-control" placeholder="Password" @if(empty($edit->user_password)) required @endif>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <h3 class="card-title">Account Details</h3>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>First Name (Required)</label>
                    			<input type="text" name="user[user_fname]" value="{{ @$edit->user_fname }}" placeholder="First Name" class="form-control name" required autocomplete="new_name">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Last Name</label>
                    			<input type="text" name="user[user_lname]" value="{{ @$edit->user_lname }}" placeholder="Last Name" class="form-control name" autocomplete="new_name">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Date Of Birth</label>
                                <input type="text" name="user[user_dob]" value="{{ @$edit->user_dob }}" class="form-control datepicker_no_future" placeholder="yyyy-mm-dd" readonly>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Date Of Aniversary</label>
                                <input type="text" name="user[user_doa]" value="{{ @$edit->user_doa }}" class="form-control datepicker_no_future" placeholder="yyyy-mm-dd" readonly>
                            </div>
                        </div>
                    </div>
                    @if($role->role_slug == 'staff')
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Department</label>
                                <select class="form-control" name="user[user_department]">
                                    <option value="">Select Department</option>
                                    @if(!$departments->isEmpty())
                                        @foreach($departments as $dept)
                                            <option value="{{ $dept->dept_name }}" @if(@$edit->user_department == $dept->dept_name) selected @endif>{{ $dept->dept_name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Date Of Joining</label>
                                <input type="text" name="user[user_doj]" value="{{ @$edit->user_doj }}" class="form-control datepicker_no_future" placeholder="yyyy-mm-dd" readonly>
                            </div>
                        </div>
                    </div>
                    @endif
                    @if($role->role_slug == "hotel")
                    <div class="form-group">
                        <label>Hotel Name (Required)</label>
                        <input type="text" name="hotel[hotel_name]" value="{{ @$edit->hotel->hotel_name }}" class="form-control" placeholder="Hotel Name" required>
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Mobile No. (Required)</label>
                    			<input type="tel" name="user[user_mobile]" value="{{ @$edit->user_mobile }}" placeholder="Mobile No." class="form-control" required autocomplete="new_mobile">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Email ID</label>
                    			<input type="email" name="user[user_email]" value="{{ @$edit->user_email }}" placeholder="Email ID" class="form-control" autocomplete="new_email">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Address Line 1 (Required)</label>
                        <input type="text" maxlength="100" name="user[user_address1]" value="{{ @$edit->user_address1 }}" class="form-control" placeholder="Address Line 1" required>
                    </div>
                    <div class="form-group">
                        <label>Address Line 2</label>
                        <input type="text" maxlength="100" name="user[user_address2]" value="{{ @$edit->user_address2 }}" class="form-control" placeholder="Address Line 2">
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Country (Required)</label>
                    			<select class="form-control country" name="user[user_country]" data-target="#userState" required>
                                    <option value="">Select Country</option>
                                    @if(!$countries->isEmpty())
                                        @foreach($countries as $c)
                                            <option value="{{ $c->country_id }}" @if($c->country_id == @$edit->user_country) selected @endif>{{ $c->country_name.' ('.$c->country_short_name.')' }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>State (Required)</label>
                                <select class="form-control state" name="user[user_state]" data-target="#userCity" id="userState" required>
                                    <option value="">Select State</option>
                                    @if(!empty($states) && !$states->isEmpty())
                                        @foreach($states as $s)
                                            <option value="{{ $s->state_id }}" @if($s->state_id == @$edit->user_state) selected @endif>{{ $s->state_name.' ('.$s->state_short_name.')' }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>City</label>
                    			<select class="form-control city" data-target="#userPincode" name="user[user_city]" id="userCity">
                                    <option value="">Select City</option>
                                    @if(!empty($cities) && !$cities->isEmpty())
                                        @foreach($cities as $c)
                                            <option value="{{ $c->city_id }}" @if($c->city_id == @$edit->user_city) selected @endif>{{ $c->city_name.' ('.$c->city_short_name.')' }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Pincode</label>
                                <select class="form-control" id="userPincode" name="user[user_pincode]">
                                    <option value="">Select Pincode</option>
                                    @if(!empty($pincodes) && !$pincodes->isEmpty())
                                        @foreach($pincodes as $p)
                                            <option value="{{ $p->pin_id }}" @if($p->pin_id == @$edit->user_pincode) selected @endif>{{ $p->pin_code }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    @if($role->role_slug == "staff")
                    <div class="form-group">
                        <label>Remarks</label>
                        <textarea name="user[user_remark]" rows="8" class="form-control" placeholder="Remarks">{{ @$edit->user_remark }}</textarea>
                    </div>
                    @endif
        		</div>
            </div>
            <div class="col-sm-4 col-lg-3">
                <div class="card">
                    <button type="submit" class="btn btn-block btn-primary">Submit</button>
                </div>
                <div class="card">
                    <h3 class="card-title">Upload Profile Image</h3>
                    <label class="upload_image">
                        <img src="{{ empty($edit->user_image) ? url('imgs/no-image.png') : url("imgs/users/{$company}/".$edit->user_image) }}" alt="Upload Image" title="Upload Image">
                        <input type="file" name="user_image" accept="image/*" id="userImage">
                    </label>
                    <label for="userImage" class="btn btn-primary btn-block">Choose Image</label>
                </div>
            </div>
        {{ Form::close() }}
    </div>
</div>
