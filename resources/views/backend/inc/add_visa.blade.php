<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1><i class="icon-plane"></i> {{ !empty($edit->visa_id) ? 'Edit ' : 'Add ' }} Visa</h1>
                <ul class="page-breadcrumb">
                    <li><a href="{{ url($company) }}"><i class="icon-dashboard"></i> Dashboard</a></li>
                    <li><a href="{{ url($company.'/visa') }}">Visa</a></li>
                    <li class="active">{{ !empty($edit->visa_id) ? 'Edit ' : 'Add ' }} Visa</li>
                </ul>
            </div>
            <div class="float-right">
                <a href="{{ url($company.'/visa/') }}" class="btn btn-primary">View Visa</a>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    {{ Form::open(["files" => true]) }}
        @if (\Session::has('danger'))
            <div class="alert alert-danger">
                {!! \Session::get('danger') !!}</li>
            </div>
        @endif

        <div class="card">
            <h3>Visa Lead</h3>
            <div class="row">
                <div class="col-sm-4 offset-sm-4">
                    <div class="form-group">
                        <select class="form-control" name="" data-placeholder="Enter Lead, Customer Name, Mobile">
                            <option value="">Select Lead</option>
                            @foreach($leads as $l)
                            <option value="{{ $l->lead_id }}">{{ sprintf('#%06d', $l->lead_id) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>

    	<div class="row d-none">
    		<div class="col-sm-8 col-lg-9">
                <div class="card">
                    <h3 class="card-title">Visa Information</h3>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Customer (Required)</label>
                                <select class="form-control" name="record[visa_customer]" required>
                                    <option value="">Select Customer</option>
                                    @foreach($customers as $c)
                                    <option value="{{ $c->user_id }}">{{ $c->user_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label>Purpose Of Travel</label>
                                <input type="text" name="record[visa_travel_purpose]" value="{{ @$edit->visa_travel_purpose }}" class="form-control" placeholder="Purpose Of Travel">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Country (Required)</label>
                    			<select class="form-control" name="record[visa_destination]" required>
                                    <option value="">Select Country</option>
                                    @if(!$countries->isEmpty())
                                        @foreach($countries as $c)
                                            <option value="{{ $c->country_id }}" @if(@$edit->visa_destination == $c->country_id) selected @endif>{{ $c->country_name.' ('.$c->country_short_name.')' }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Visa Type (Required)</label>
                                <select class="form-control" name="record[visa_type]" required>
                                    <option value="">Select Visa Type</option>
                                    <option value="Student Visa" @if(@$edit->visa_type == "Student Visa") selected @endif>Student Visa</option>
                                    <option value="Business Visa" @if(@$edit->visa_type == "Business Visa") selected @endif>Business Visa</option>
                                    <option value="Tourist Visa" @if(@$edit->visa_type == "Tourist Visa") selected @endif>Tourist Visa</option>
                                    <option value="Other" @if(@$edit->visa_type == "Other") selected @endif>Other</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>No. Of Days (Required)</label>
                                <input type="number" name="record[visa_days]" value="{{ @$edit->visa_days }}" class="form-control" placeholder="No. Of Days" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>No. Of Adult (Required)</label>
                                <input type="number" min="1" name="record[visa_adult_no]" value="{{ @$edit->visa_adult_no }}" class="form-control" placeholder="No. Of Adult" required>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>No. Of Kids (Required)</label>
                                <input type="number" min="0" name="record[visa_kid_no]" value="{{ @$edit->visa_kid_no }}" class="form-control" placeholder="No. Of Kids" required>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>No. Of Infant (Required)</label>
                                <input type="number" min="0" name="record[visa_infant]" value="{{ @$edit->visa_infant }}" class="form-control" placeholder="No. Of Infant" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Remarks</label>
                        <textarea name="record[visa_remark]" class="form-control" rows="8" placeholder="Remarks">{!! @$edit->visa_remark !!}</textarea>
                    </div>
        		</div>
            </div>
            <div class="col-sm-4 col-lg-3">
                <div class="card">
                    <button type="submit" class="btn btn-block btn-primary">Submit</button>
                </div>
            </div>
        {{ Form::close() }}
    </div>
</div>
