<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1><i class="icon-user2"></i> {{ $role->role_name }} Account</h1>
                <ul class="page-breadcrumb">
                    <li><a href="{{ url($company) }}"><i class="icon-dashboard"></i> Dashboard</a></li>
                    <li class="active">{{ $role->role_name }} Account</li>
                </ul>
            </div>
            <div class="float-right">
                <a href="{{ url($company.'/account/'.$role->role_slug.'/add') }}" class="btn btn-primary">Add New</a>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    @if (\Session::has('success'))
    <div class="alert alert-success">
        {!! \Session::get('success') !!}
    </div>
    @endif
    <div class="card">
        <h3 class="card-title"><i class="icon-filter1"></i> Filter By</h3>
        {{ Form::open(['method' => 'GET']) }}
        <div class="row">
            <div class="col-sm-2">
                <div class="form-group">
                    <input type="text" name="search[keyword]" value="{{ @$search['keyword'] }}" class="form-control" placeholder="By keywords">
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <select name="search[country]" class="form-control country" data-target="#searchState">
                        <option value="">Select Country</option>
                        @foreach($countries as $con)
                            <option value="{{ $con->country_id }}" @if(@$search['country'] == $con->country_id) selected @endif>{{ $con->country_name.' ('.$con->country_short_name.')' }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <select name="search[state]" class="form-control state" data-target="#searchCity" id="searchState">
                       <option value="">Select State</option>
                       @foreach($states as $st)
                       <option value="{{ $st->state_id }}" @if(@$search['state'] == $st->state_id) selected @endif>{{ $st->state_name.' ('.$st->state_short_name.')' }}</option>
                       @endforeach
                    </select>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <select name="search[city]" class="form-control city" data-target="#searchPincode" id="searchCity">
                       <option value="">Select City</option>
                       @foreach($cities as $ct)
                       <option value="{{ $ct->city_id }}" @if(@$search['city'] == $ct->city_id) selected @endif>{{ $ct->city_name.' ('.$ct->city_short_name.')' }}</option>
                       @endforeach
                    </select>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <select name="search[pincode]" class="form-control" id="searchPincode">
                       <option value="">Select Pincode</option>
                       @foreach($pincodes as $p)
                       <option value="{{ $p->pin_id }}" @if(@$search['pincode'] == $p->pin_id) selected @endif>{{ $p->pin_code }}</option>
                       @endforeach
                    </select>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block"><i class="icon-search"></i> Search</button>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
	<div class="card">
        {{ Form::open() }}
            <a href="#remove" class="float-right" data-toggle="tooltip" title="Remove Selected"> <i class="icon-trash-o"></i> </a>
    		<h3 class="card-title">
                <div class="mr-auto"><i class="icon-user2"></i> View {{ $role->role_name }} Account</div>
    		</h3>
		    @if(!$records->isEmpty())
		    <div class="table-responsive">
			    <table class="table table-bordered">
			          <thead>
			               <tr>
			                    <th style="width: 50px;">
			                        <label class="animated-checkbox">
			                            <input type="checkbox" class="checkall">
			                            <span class="label-text"></span>
			                        </label>
			                    </th>
			                   <th style="width: 50px;">S.No.</th>
                               <th>Image</th>
                               <th>Login ID</th>
                               <th>Name</th>
			                   <th>Mobile No.</th>
			                   <th>Email Address</th>
                               @if($role->role_slug == "hotel")
                               <th>Hotel Name</th>
                               @endif
                               <th>Address</th>
                               <th>Action</th>
			               </tr>
			          </thead>

			          <tbody>
			          		@php $sn = $records->firstItem(); @endphp
			          		@foreach($records as $rec)
                            @php
                                $image   = !empty( $rec->user_image ) ? url( "/imgs/users/{$company}/".$rec->user_image ) : url( 'imgs/no-image.png' );
                                $address = $rec->user_address1;
                                if(!empty($rec->user_address2))
                                    $address .= "<br>".$rec->user_address2;

                                if(!empty($rec->city->city_name))
                                    $address .= "<br>".$rec->city->city_name;

                                if(!empty($rec->state->state_name))
                                    $address .= ", ".$rec->state->state_name;

                                if(!empty($rec->country->country_name))
                                    $address .= ", ".$rec->country->country_name;

                                if(!empty($rec->pincode->pin_code))
                                    $address .= ". ".$rec->pincode->pin_code;
                            @endphp
			               	<tr>
		                        <td>
		                            <label class="animated-checkbox">
		                                <input type="checkbox" name="check[]" value="{{ $rec->country_id  }}" class="check @if($rec->states_count || $rec->cities_count) disabled @endif" @if($rec->states_count || $rec->cities_count) disabled @endif>
		                                <span class="label-text"></span>
		                            </label>
		                        </td>
								<td>{{ $sn++ }}</td>
                                <td> <img src="{{ $image }}" alt="{{ $rec->user_name }}" title="{{ $rec->user_name }}" class="table-img"> </td>
                                <td>
                                    <a href="{{ url($company.'/account/'.$role->role_slug.'/add/'.$rec->user_id) }}" class="pencil">
                                        <i class="icon-pencil" title="Edit"></i> {{ $rec->user_login }} ({{ sprintf("%06d", $rec->user_id)  }})
                                    </a>
                                </td>
								<td>{{ $rec->user_name }}</td>
                                <td>{{ $rec->user_mobile }}</td>
                                <td>{{ $rec->user_email }}</td>
                                @if($role->role_slug == "hotel")
                                <td>{{ $rec->hotel->hotel_name }}</td>
                                @endif
								<td>{!! $address !!}</td>
                                <td>
                                    <select class="action-dropdown" data-url="{{ url($company.'/account/'.$role->role_slug.'/?id='.$rec->user_id) }}" data-enable="{{ $rec->user_is_blocked }}">
                                        <option value="disable"@if($rec->user_is_blocked == "N") selected @endif>Block</option>
                                        <option value="enable"@if($rec->user_is_blocked == "Y") selected @endif>Unblock</option>
                                    </select>
                                </td>
			               	</tr>
			               @endforeach
			          </tbody>
			    </table>
			</div>
            @php
                $get_param = request()->input();
                if(isset($get_param['page'])) {
                    unset($get_param['page']);
                }
            @endphp
            {{ $records->appends($get_param)->links() }}
		    @else
		    <div class="no_records_found">
		      No records found yet.
		    </div>
			@endif
		{{ Form::close() }}
	</div>
</div>
