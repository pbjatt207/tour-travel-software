<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title>{{ $company_info->company_name }}</title>

        {{ HTML::style('css/bootstrap.min.css') }}
        {{ HTML::style('icomoon/style.css') }}
        {{ HTML::style('admin/css/style.css') }}
        {{ HTML::style('admin/css/theme/blue.css') }}

        <link rel="icon" href="{{ url('imgs/companies/'.$company_info->company_favicon) }}">
    </head>
    <body>
    <section class="login-page">
    	<input type="hidden" id="base_url" value="{{ url($company_info->company_sitename) }}">
        <div class="half"></div>
        <div class="container">
            <form id="loginForm" method="post" action="{{ url($company_info->company_sitename.'/ajax/user_login') }}">
            @csrf
            <input type="hidden" name="record[user_company]" value="{{ $company_info->company_uid }}">
            <div class="sec">
                <h3 class="text-center mb-3">
                    <div class="site_logo">
                        <img src="{{ url('imgs/companies/'.$company_info->company_logo) }}" alt="{{ $company_info->company_name }}" title="{{ $company_info->company_name }}">
                    </div>
                    {{ $company_info->company_name }}
                </h3>
                <div class="form-msg"></div>
                <div class="form-group">
                    <label>Username</label>
                    <input type="text" name="record[user_login]" class="form-control rad" placeholder="Username" autocomplete="off" required>
                </div>

                <div class="form-group">
                    <label>Password</label>
                    <input type="password" name="record[user_password]" class="form-control rad" placeholder="Password" autocomplete="new-password" required>
                </div>

                <div class="form-group">
                    <a href="#">Forgot Password?</a>
                </div>

                <div class="third">
                    <button type="submit" class="btn btn-primary btn-block third">Sign In</button>
                </div>


            </div>
            </form>
        </div>
    </section>

    <div id="loadingBox">
        <div class="loading-bg"></div>
        <div class="loading-body">
            <div class="loader-border">
                <img src="{{ url('imgs/companies/'.$company_info->company_logo) }}" alt="Company Logo" title="Company Logo">
            </div>
        </div>
    </div>

    {{ HTML::script('js/jquery.min.js') }}
    {{ HTML::script('js/popper.min.js') }}
    {{ HTML::script('js/bootstrap.min.js') }}
    {{ HTML::script('js/sweetalert.min.js') }}
    {{ HTML::script('js/validation.js') }}
    {{ HTML::script('js/jquery-ui.js') }}
    {{ HTML::script('admin/js/select2.min.js') }}
    {{ HTML::script('admin/tinymce/js/tinymce/tinymce.min.js') }}
    {{ HTML::script('admin/js/main.js') }}
    </body>
</html>
