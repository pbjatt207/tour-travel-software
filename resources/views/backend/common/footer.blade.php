			</div>
		</div>
	</div>

	<div id="loadingBox">
		<div class="loading-bg"></div>
		<div class="loading-body">
			<div class="loader-border">
				<img src="{{ url('imgs/companies/'.$company_info->company_logo) }}" alt="Company Logo" title="Company Logo">
			</div>
		</div>
	</div>

	{{ HTML::script('js/jquery.min.js') }}
	{{ HTML::script('js/popper.min.js') }}
	{{ HTML::script('js/bootstrap.min.js') }}
	<!-- {{ HTML::script('js/owl.carousel.min.js') }} -->
	{{ HTML::script('js/sweetalert.min.js') }}
	<!-- {{ HTML::script('js/bootstrap-tagsinput.min.js') }} -->
	{{ HTML::script('js/validation.js') }}
	{{ HTML::script('js/jquery-ui.js') }}
	{{ HTML::script('js/moment.js') }}
	<!-- {{ HTML::script('js/bootstrap-tagsinput-angular.min.js') }} -->
	{{ HTML::script('admin/tinymce/js/tinymce/tinymce.min.js') }}
	{{ HTML::script('admin/js/select2.min.js') }}
	{{ HTML::script('admin/js/main.js') }}
</body>
</html>
