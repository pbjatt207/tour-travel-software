<div>
    <div class="row">
        <div class="col-8">
            <div style="max-height: 400px; overflow: auto;">
                @if(!$records->isEmpty())
                    {{ Form::open() }}
                    @foreach($records as $rec)
        		    <div class="card">
                        <div class="row">
                            <div class="col-4" style="white-space: nowrap;">
                                <strong>Date</strong> {{ date('d/m/Y', strtotime($rec->lfu_date)) }}
                            </div>
                            <div class="col-4" style="white-space: nowrap;">
                                <strong>Next Date</strong> {{ date('d/m/Y', strtotime($rec->lfu_next_date)) }}
                            </div>
                            <div class="col-4" style="white-space: nowrap;">
                                <i class="icon-user"></i> {{ $rec->lead->lead_customer_name }}
                            </div>
                        </div>

                        <div class="mt-2">
                            {!! $rec->lfu_message !!}
                        </div>
        			</div>
                    @endforeach
        		    @else
        		    <div class="no_records_found">
        		      No records found yet.
        		    </div>
        			@endif
        		{{ Form::close() }}
            </div>
        </div>
        <div class="col-4">
            <div class="card">
                <h3 class="card-title">Follow-up Message</h3>
                <input type="hidden" name="record[lfu_date]" value="{{ date('Y-m-d') }}">
                <input type="hidden" name="record[lfu_lid]" value="{{ $lead_id }}">
                <div class="form-group">
                    <label>Next Follow-up Date</label>
                    <input type="text" name="record[lfu_next_date]" value="" class="form-control datepicker_no_past" placeholder="yyyy-mm-dd" readonly required>
                </div>
                <div class="form-group">
                    <label>Contact Mode</label>
                    <select class="form-control" name="record[lfu_contact_mode]" required>
                        <option value="">Select Mode</option>
                        <option value="Email">Email</option>
                        <option value="Whatsapp">Whatsapp</option>
                        <option value="SMS">SMS</option>
                        <option value="Call">Call</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Message</label>
                    <textarea name="record[lfu_message]" rows="5" class="form-control" placeholder="Write some message" required></textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-block btn-primary">Submit <i class="icon-long-arrow-right"></i> </button>
                </div>
            </div>
        </div>
    </div>
</div>
