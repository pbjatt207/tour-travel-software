@php
    $company_url  = Request()->company;
    $company_info = App\Model\Company::where('company_sitename', 'LIKE', $company_url)->first();
    if( empty($company_info->company_id) ) {
        echo '<script>window.location = "'.url('404-error').'"</script>';
        die;
    }

    $company = $company_info->company_sitename;
@endphp

@if(session()->has('user_auth'))

    @php
        $profile = App\Model\User::find(session('user_auth'));
    @endphp

    @include('backend.common.header')

    @include('backend.inc.'.$page)

    @include('backend.common.footer')

@else

    @include('backend.login')

@endif
