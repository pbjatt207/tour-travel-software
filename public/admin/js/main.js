$(function() {
      var baseurl   = $("#base_url").val();

      // Ajax Request
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });

      $('a[href="#"]').on('click', function(e) {
          e.preventDefault();
      });

      $(document).on('click', '[data-toggle="popup-open"]', function(e) {
          e.preventDefault();

          var target = $(this).attr('href');

          $(target).toggleClass('show');
      });

      $(document).on('click', '.follow-popup-btn', function() {
          $('#lead-follow-popup').addClass('show');

          var lead_id = $(this).data('id');

          $('#lead-follow-popup .follow-response').html( '<div class="text-center">Loading</div>' );

          $.ajax({
              url: baseurl+'/ajax/follow_up',
              type: 'POST',
              data: {
                  id: lead_id
              },
              success: function(res) {
                  $('#lead-follow-popup .follow-response').html( res.html );

                  $( ".datepicker_no_past" ).datepicker({
                      changeMonth: true,
                      changeYear: true,
                      dateFormat: 'yy-mm-dd',
                      minDate: 0
                  });
              }
          });
      });

      $(document).on('click', '[name="create_account"]', function(e) {
          if($(this).prop('checked')) {
              $('.user_data').removeClass('d-none');
              $('.user_data .form-control').attr('required', 'required');
          } else {
              $('.user_data').addClass('d-none');
              $('.user_data .form-control').removeAttr('required', 'required');
          }
      });

      $(document).on('change', '#select_staff', function() {
          var form = $(this).closest('form');

          if($(this).val() != '') {

              if(form.find(".check:checked").length > 0) {
                  form.trigger('submit');
              } else {
                  swal("Warning", "Select at least one record to assign.", "warning");

                  $(this).val('').trigger('change');
              }
          }
      });

      $(document).on('change', '#leadAccount', function() {
          var name      = $('[name="record[lead_customer_name]"]'),
              email     = $('[name="record[lead_customer_email]"]'),
              mob       = $('[name="record[lead_contact_no]"]'),
              phone     = $('[name="record[lead_phone]"]'),
              addr1     = $('[name="record[lead_address1]"]'),
              addr2     = $('[name="record[lead_address2]"]'),
              contry    = $('[name="record[lead_country]"]'),
              state     = $('[name="record[lead_state]"]'),
              city      = $('[name="record[lead_city]"]'),
              pincode   = $('[name="record[lead_pincode]"]');

          if($(this).val() != '') {
              var ajax_url = $(this).data('url');

              $('.loadingBox').show();
              $.ajax({
                  url: ajax_url,
                  type: 'POST',
                  data: {
                      id: $(this).val()
                  },
                  success: function(res) {
                      $('.loadingBox').hide();
                      if(res.status) {
                          name.val( res.data.user_name );
                          email.val( res.data.user_email );
                          mob.val( res.data.user_mobile );
                          phone.val( res.data.user_phone );
                          addr1.val( res.data.user_address1 );
                          addr2.val( res.data.user_address2 );
                          contry.val( res.data.user_country );
                          state.val( res.data.user_state ).trigger('change');
                          setTimeout(function() {
                              city.val( res.data.user_city ).trigger('change');
                          }, 500);

                          setTimeout(function() {
                              pincode.val( res.data.user_pincode ).trigger('change');
                          }, 1000);
                      }
                  }
              });
          } else {

          }
      });

      $(document).on('click', '.popup-bg', function(e) {
          e.preventDefault();

          var target = $(this).closest('.popup-container');

          $(target).removeClass('show');
      });

      $('select.form-control').select2();

      $(document).on('change', '.import_csv input[type=file]', function() {
          $(this).closest('form').trigger('submit');
      });

      // Datepicker
      $( ".tavel_date" ).datepicker({
          changeMonth: true,
          changeYear: true,
          dateFormat: 'dd M yy',
          altField: '#alt_travel_date',
          altFormat: 'yy-mm-dd',
          minDate: 0
      });

      $( ".datepicker" ).datepicker({
          changeMonth: true,
          changeYear: true,
          dateFormat: 'yy-mm-dd',
      });

      $( ".datepicker_no_past" ).datepicker({
          changeMonth: true,
          changeYear: true,
          dateFormat: 'yy-mm-dd',
          minDate: 0
      });

      $( ".datepicker_no_future" ).datepicker({
          changeMonth: true,
          changeYear: true,
          dateFormat: 'yy-mm-dd',
          maxDate: 0
      });

      $( ".datepicker_dob" ).datepicker({
          changeMonth: true,
          changeYear: true,
          dateFormat: 'yy-mm-dd',
          maxDate: -3650
      });
      // End Datepicker

      $('[data-toggle="tooltip"]').tooltip();

      $('a[href="#refresh"]').on('click', function(e) {
          e.preventDefault();

          location.reload();
      });

      $('a[href="#save-data"]').on('click', function(e) {
          e.preventDefault();

          $(this).closest('form').trigger('submit');
      });

      $(".checkall").on("click", function() {
          var form = $(this).closest("form");

          form.find(".check:not([disabled])").prop( "checked", $(this).prop("checked") );
      });

      $(".check").on("click", function() {
          var form = $(this).closest("form");
          var checked = (form.find('.check').length == form.find('.check:checked').length);

          form.find(".checkall").prop( "checked", checked );
      });

      $("a[href='#remove']").on("click", function (e) {
          e.preventDefault();

          var form = $(this).closest("form");
          if(form.find(".check:checked").length > 0) {
              swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover record(s)!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
              })
                  .then((willDelete) => {
                      if (willDelete) {
                          form.trigger("submit");
                      }
                  });
          } else {
              swal("Warning", "Select at least one record to delete", "warning");
          }

      });
      var i = 0;
      $(".action-dropdown").on("change", function (e) { i++;
          var sel    = $(this),
              action = sel.val(),
              url    = sel.data('url'),
              enable = sel.data('enable');

          if(action != '' && i%2 == 1) {
              swal({
                  title: "Are you sure?",
                  text: "You're going to "+action+" record",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
              }).then((willDelete) => {
                  if(willDelete) {
                      window.location = url+'&action='+action;
                  } else {
                      if(enable == 'N') {
                          sel.val( 'enable' ).trigger('change');
                      } else {
                          sel.val( 'disable' ).trigger('change');
                      }
                  }
              });
          }

      });

      $(".upload_image input[type=file]").change(function() {
          var target = $(this).closest(".upload_image").find("img");
          readURL(this, target);
      });

      $("#loginForm").on("submit", function(e) {
          e.preventDefault();

          var form     = $(this);
          var is_valid = form.is_valid();
          var fmsg     = form.find('.form-msg');
          var action   = form.attr('action');

          if(is_valid) {
              fmsg.addClass('alert alert-info').removeClass('alert-danger alert-success').html('Progressing, please wait...');

              $.ajax({
                  url: action,
                  type: 'POST',
                  data: form.serialize(),
                  success: function(res) {
                      if(res.status) {
                          fmsg.removeClass('alert-info').addClass('alert-success').html(res.message);
                          location.href = "";
                      } else {
                          fmsg.removeClass('alert-info').addClass('alert-danger').html(res.message);
                      }
                  }
              });
          }
      });

      $("#addCustomerForm").on("submit", function(e) {
          e.preventDefault();

          var form     = $(this);
          var is_valid = form.is_valid();
          var fmsg     = form.find('.form-msg');
          var action   = form.attr('action');

          var target   = form.data('target');

          if(is_valid) {
              fmsg.addClass('alert alert-info').removeClass('alert-danger alert-success').html('Progressing, please wait...');

              $.ajax({
                  url: action,
                  type: 'POST',
                  data: form.serialize(),
                  success: function(res) {
                      if(res.status) {
                          fmsg.removeClass('alert alert-info').html('');

                          form.closest('.popup-container').removeClass('show');

                          $(target).append( '<option value="'+res.user_id+'">'+res.user_name+' '+res.user_mobile+'</option>' ).val(res.user_id).trigger('change');
                      } else {
                          fmsg.removeClass('alert-info').addClass('alert-danger').html(res.message);
                      }
                  }
              });
          }
      });

      tinyMCE.PluginManager.add('stylebuttons', function(editor, url) {
        ['div', 'pre', 'p', 'code', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'].forEach(function(name){
          editor.addButton("style-" + name, {
              tooltip: "Toggle " + name,
                text: name.toUpperCase(),
                onClick: function() { editor.execCommand('mceToggleFormat', false, name); },
                onPostRender: function() {
                    var self = this, setup = function() {
                        editor.formatter.formatChanged(name, function(state) {
                            self.active(state);
                        });
                    };
                    editor.formatter ? setup() : editor.on('init', setup);
              }
          })
        });
      });
      tinymce.init({
          selector: '.editor',
          plugins: "code, link, image, textcolor, emoticons, hr, lists, stylebuttons, charmap",
          fontsizeselect: true,
          browser_spellcheck: true,
          menubar: false,
          toolbar: 'bold italic underline strikethrough | style-h1 style-h2 style-h3 | hr superscript subscript | alignleft aligncenter alignright alignjustify bullist numlist outdent indent code' ,
          // forced_root_block : 'div',
          branding: false,
          protect: [
              /\<\/?(if|endif)\>/g,  // Protect <if> & </endif>
              /\<xsl\:[^>]+\>/g,  // Protect <xsl:...>
              /\<script\:[^>]+\>/g,  // Protect <xsl:...>
              /<\?php.*?\?>/g  // Protect php code
          ],
          images_upload_credentials: true,
          file_browser_callback_types: 'image',
          image_dimensions: true,
          automatic_uploads: true,
          relative_urls : false,
          remove_script_host : false,
          images_reuse_filename: true,
      });

      // Custom JS
      // Locations
      $('.country').on('change', function() {
          var target = $(this).data('target');

          $(target).attr('disabled', 'disabled').html( $('<option>').val('').text('Loading') );

          $.ajax({
              url: baseurl+'/ajax/get_states/',
              type: 'POST',
              data: {
                  'id': $(this).val()
              },
              success: function(res) {
                  $(target).removeAttr('disabled');
                  $(target).html( $('<option>').val('').text('Select State') );

                  $.each(res.data, function(i, row) {
                      $(target).append( $('<option>').val(row.state_id).text(row.state_name+' ('+row.state_short_name+')') );
                  });

                  $(target).trigger('change');
              }
          });
      });
      $('.state').on('change', function() {
          var target = $(this).data('target');

          $(target).attr('disabled', 'disabled').html( $('<option>').val('').text('Loading') );

          $.ajax({
              url: baseurl+'/ajax/get_cities/',
              type: 'POST',
              data: {
                  'id': $(this).val()
              },
              success: function(res) {
                  $(target).removeAttr('disabled');
                  $(target).html( $('<option>').val('').text('Select City') );

                  $.each(res.data, function(i, row) {
                      $(target).append( $('<option>').val(row.city_id).text(row.city_name+' ('+row.city_short_name+')') );
                  });

                  $(target).trigger('change');
              }
          });
      });
      $('.city').on('change', function() {
          var target = $(this).data('target');

          $(target).attr('disabled', 'disabled').html( $('<option>').val('').text('Loading') );

          $.ajax({
              url: baseurl+'/ajax/get_pincodes/',
              type: 'POST',
              data: {
                  'id': $(this).val()
              },
              success: function(res) {
                  $(target).removeAttr('disabled');
                  $(target).html( $('<option>').val('').text('Select Pincode') );

                  $.each(res.data, function(i, row) {
                      $(target).append( $('<option>').val(row.pin_id).text(row.pin_code) );
                  });

                  $(target).trigger('change');
              }
          });
      });
      // End Location

      $('#add_destination').on('click', function() {
          $('#loadingBox').show();
          var cn = $('#destCountry').val(),
              st = $('#destState').val(),
              ct = $('#destCity').val();

          if(cn != '') {
              $.ajax({
                  url: baseurl+'/ajax/save_destination_sess/',
                  type: 'POST',
                  data: {
                      country: cn,
                      state: st,
                      city: ct
                  },
                  success: function(res) {
                      $('#destinationList').html( res.html );

                      $('#loadingBox').hide();
                  }
              });
          }
      });

      $(window).on('load', function() {
          $('#loadingBox').hide();
      });

      $('#lead_visa').on('change', function() {
          $('#loadingBox').show();
          var sel = $(this).val();

          $('.lead_data').find('.form-control').removeAttr('required');
          if(sel != '') {
              sel = "."+sel.toLowerCase().replace(" ", "-");

              $('.lead_data').not(sel).hide();
              $(sel).show();

              $(sel).find('.form-control').attr('required', 'required');
          } else {
              $('.lead_data').hide();
          }
          $('#loadingBox').hide();
      });

      $(document).on('click', '.num-input-change .plus', function() {
          var input = $(this).closest( '.number-input' ).find('.form-control');

          input.val( parseInt( input.val() ) + 1 );
      });

      $(document).on('click', '.num-input-change .minus', function() {
          var input = $(this).closest( '.number-input' ).find('.form-control');
          var min_value = input.attr('min');
          min_value = min_value != '' && min_value != undefined ? min_value : 0;

          if(input.val() > min_value) {
              input.val( parseInt( input.val() ) - 1 );
          }
      });

      $(document).on('click', '.spinner .add', function() {
          var input     = $(this).closest( '.plus-minus' ).find('.night-count');
          var new_value = parseInt( input.text() ) + 1;
          input.text( new_value ).data('nights', new_value);

          $(this).closest('.plus-minus').find('.no_of_nights').val( new_value );

          update();
      });

      $(document).on('click', '.spinner .sub', function() {
          var input = $(this).closest( '.plus-minus' ).find('.night-count');
          var min_value = 1;

          if(input.text() > min_value) {
              var new_value = parseInt( input.text() ) - 1
              input.text( new_value ).data('nights', new_value);

              $(this).closest('.plus-minus').find('.no_of_nights').val( new_value );

              update();
          }
      });

      $(document).on('click', '.vertical-tabs a', function(e) {
          e.preventDefault();
          var sel = $(this).attr('href');

          $('.hotel-place').not(sel).removeClass('active');
          $(sel).addClass('active');

          $('.vertical-tabs li a').not(this).closest('li').removeClass('active');
          $(this).closest('li').addClass('active');
      });

        $(".sortable").sortable({
            handle: '.handle',
            placeholder: 'sort-placeholder',
            forcePlaceholderSize: true,
            update: function (e, ui) {
                update();
            }
        });

        $(document).on('click', '.deleteMe [class^=icon-]', function() {
            $(this).closest('li').remove();

            var new_value = $('#total_place').val() - 1;
            $('#total_place').val( new_value );
            update();
        });

        $('#travelForm').on('submit', function(e) {
            is_valid = $(this).is_valid();

            if($('#total_place').val() == 0 && is_valid) {
                e.preventDefault();
                swal("Error!", "Please select at least one travel place!", "warning").then(function() {
                    $('#visitPlace').focus();
                });

            }
        });
        // End Custom Js
});

function update() {
    var d = $('#travel_date').val();
    var startDate = moment(d, "DD MMM YYYY").format('DD MM YYYY');
    var preCheckout = null;
    $('#buildtour > ul > li span.night-count').each(function () {
        var night = parseInt($(this).data('nights'));
        var row = $(this).closest('li.visit'); //parent().parent().parent()
        if (preCheckout) {
            var checkIn = preCheckout;
            row.find('[data-checkin]').text(moment(checkIn, 'DD MM YYYY').format('DD MMM'));
            var checkOut = moment(checkIn, 'DD MM YYYY').add(night, 'd').format('DD MM YYYY');
        } else { // first time
            var checkIn = startDate;
            row.find('[data-checkin]').text(moment(checkIn, 'DD MM YYYY').format('DD MMM'));
            var checkOut = moment(checkIn, 'DD MM YYYY').add(night, 'd').format('DD MM YYYY');
        }
        preCheckout = checkOut;
        row.find('[data-checkout]').text(moment(checkOut, 'DD MM YYYY').format('DD MMM'));
    });
}

function readURL(input, target) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $(target).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

// Autocmplete
var cities = [];
var get_cities_url = base_url+"/ajax/get_all_cities";
cities = fetch(get_cities_url).
    then(function(response) {
        return response.json();
    }).
    then(function(data) {
        $( ".autocomplete .form-control" ).autocomplete({
            source: data,
            autoFocus: true
        });
    });

$(document).on("autocompleteselect", "#visitPlace", function( event, ui ) {
    var html = `
        <li class="visit card">
            <div class="row">
               <div class="col-md-4 col-lg-4 col-sm-4 col-3 place">${ui.item.value}</div>
               <input type="hidden" name="record[visit_place][]" value="${ui.item.value}">
               <div class="col-md-3 col-lg-3 col-sm-3 col-4 pd-t-3">
                   <div class="row">
                      <div class="col-md-6 col-lg-6 col-sm-6 col-6 boxcolor">
                         <div class="caption caption-text"><span class="hidden-xs">Check in</span><span class="visible-xs hidden-lg hidden-md hidden-sm">In</span></div>
                         <div data-checkin="" class="font-12">13 Feb</div>
                      </div>
                      <div class="col-md-6 col-lg-6 col-sm-6 col-6 boxcolor ">
                         <div class="caption caption-text"><span class="hidden-xs">Check out</span><span class="visible-xs hidden-lg hidden-md hidden-sm">Out</span></div>
                         <div data-checkout="" class="font-12">15 Feb</div>
                      </div>
                  </div>
               </div>
               <div class="col-md-3 col-lg-3 col-sm-3 col-3">
                  <div class="nightparent btn-group">
                     <div class="btn btn-lg btncolor btn-pad hidden-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">NIGHT</div>
                     <div class="btn btn-lg plus-minus">
                         <span data-id="night" class="night-count" data-nights="2">2</span>
                         <input type="hidden" name="record[${ui.item.value}][no_of_nights]" value="2" class="no_of_nights">
                         <span class="spinner">
                             <span class="add icon-plus2"></span>
                             <span class="sub icon-minus2"></span>
                         </span>
                      </div>
                  </div>
               </div>
               <div class="col-md-1 col-lg-1 col-sm-1 col-1 icon-button handle pull-right checkinbrd">
                   <i class="icon-expand1" title="Drag"></i>
               </div>
               <div class="col-md-1 col-lg-1 col-sm-1 col-1 icon-button handle pull-right deleteMe">
                   <i class="icon-clear" title="Remove"></i>
               </div>
           </div>
        </li>
    `;

    $("#buildtour ul").append( html );

    $('#visitPlace').val( '' );

    $('#total_place').val( parseInt( $('#total_place').val() ) + 1 );

    update();
    return false;
});

// end autocomplete
