$(function () {
    var baseurl   = $("#base_url").val();

    // Ajax Request
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // Locations
    $('.country').on('change', function() {
        var target = $(this).data('target');
        $.ajax({
            url: baseurl+'/ajax/get_states/',
            type: 'POST',
            data: {
                'id': $(this).val()
            },
            success: function(res) {
                $(target).html( $('<option>').val('').text('Select State') );

                $.each(res.data, function(i, row) {
                    $(target).append( $('<option>').val(row.state_id).text(row.state_name+' ('+row.state_short_name+')') );
                });
            }
        });
    });

    $('a[rel="product_toggle"]').on('click', function(e) {
        e.preventDefault();

        var target = $(this).attr('href');
        $(target).slideToggle();

        $(this).find('i').toggleClass('icon-plus-circle icon-minus-circle text-success text-danger');
    });

    $('[data-toggle="tooltip"]').tooltip();

    $('a[href="#refresh"]').on('click', function(e) {
        e.preventDefault();

        location.reload();
    });

    $('a[href="#save-data"]').on('click', function(e) {
        e.preventDefault();

        $(this).closest('form').trigger('submit');
    });

    $('.hide_show_toggle').on('click', function(e) {
        var sel = $(this).closest('.card').find('.card-body');
        if($(this).prop('checked')) {
            sel.removeClass('d-none');
        } else {
            sel.addClass('d-none');
        }
    });

    $('.hide_show_toggle2').on('click', function(e) {
        var sel = $(this).closest('.col').find('.toggle_body');
        if($(this).prop('checked')) {
            sel.removeClass('d-none');
        } else {
            sel.addClass('d-none');
        }
    });

    // Login form ajax
    $("#loginForm").on("submit", function(e) {
        e.preventDefault();
        var form     = $(this);
        var is_valid = form.is_valid();
        var fmsg     = form.find('.form-msg');
        var action   = form.attr('action');

        if(is_valid) {
            fmsg.addClass('alert alert-info').removeClass('alert-danger alert-success').html('Progressing, please wait...');

            $.ajax({
                url: action,
                type: 'POST',
                data: form.serialize(),
                success: function(res) {
                    res = JSON.parse(res);
                    console.log(res);
                    if(res.status) {
                        fmsg.removeClass('alert-info').addClass('alert-success').html(res.message);
                        location.href = "";
                        // window.location.href = "";
                    } else {
                        fmsg.removeClass('alert-info').addClass('alert-danger').html(res.message);
                    }
                }
            });
        }
    });

    $(document).on('keyup blur', '#netWgWoodKg', function(e) {
        // 1 kilogram = 2.2046226218 pound (lbs)
        var wg_wood_kg  = $(this).val();
        var wg_wood_lbs = wg_wood_kg != '' && wg_wood_kg > 0 ? wg_wood_kg * 2.2046226218 : 0;
        wg_wood_lbs     = parseFloat( wg_wood_lbs ).toFixed(2);
        $('#netWgWoodLbs').val( wg_wood_lbs );

        calcNetWeight();
    });

    $(document).on('keyup blur', '#netWgWoodLbs', function(e) {
        // 1 kilogram = 2.2046226218 pound (lbs)
        var wg_wood_lbs     = $(this).val();
        var wg_wood_kg      = wg_wood_lbs != '' && wg_wood_lbs > 0 ? wg_wood_lbs * 0.453592 : 0;
        wg_wood_kg          = parseFloat( wg_wood_kg ).toFixed(2);
        $('#netWgWoodKg').val( wg_wood_kg );

        calcNetWeight();
    });

    $(document).on('keyup blur', '#netWgIronKg', function(e) {
        // 1 kilogram = 2.2046226218 pound (lbs)
        var wg_wood_kg  = $(this).val();
        var wg_wood_lbs = wg_wood_kg != '' && wg_wood_kg > 0 ? wg_wood_kg * 2.2046226218 : 0;
        wg_wood_lbs     = parseFloat( wg_wood_lbs ).toFixed(2);
        $('#netWgIronLbs').val( wg_wood_lbs );

        calcNetWeight();
    });

    $(document).on('keyup blur', '#netWgIronLbs', function(e) {
        // 1 kilogram = 2.2046226218 pound (lbs)
        var wg_wood_lbs     = $(this).val();
        var wg_wood_kg      = wg_wood_lbs != '' && wg_wood_lbs > 0 ? wg_wood_lbs * 0.453592 : 0;
        wg_wood_kg          = parseFloat( wg_wood_kg ).toFixed(2);
        $('#netWgIronKg').val( wg_wood_kg );

        calcNetWeight();
    });

    $(document).on('keyup blur', '#netWgOtherKg', function(e) {
        // 1 kilogram = 2.2046226218 pound (lbs)
        var wg_wood_kg  = $(this).val();
        var wg_wood_lbs = wg_wood_kg != '' && wg_wood_kg > 0 ? wg_wood_kg * 2.2046226218 : 0;
        wg_wood_lbs     = parseFloat( wg_wood_lbs ).toFixed(2);
        $('#netWgOtherLbs').val( wg_wood_lbs );

        calcNetWeight();
    });

    $(document).on('keyup blur', '#netWgOtherLbs', function(e) {
        // 1 kilogram = 2.2046226218 pound (lbs)
        var wg_wood_lbs     = $(this).val();
        var wg_wood_kg      = wg_wood_lbs != '' && wg_wood_lbs > 0 ? wg_wood_lbs * 0.453592 : 0;
        wg_wood_kg          = parseFloat( wg_wood_kg ).toFixed(2);
        $('#netWgOtherKg').val( wg_wood_kg );

        calcNetWeight();
    });

    $(document).on('keyup blur', '#productCbm', function(e) {
        var pro_cbm = $(this).val();
        pro_cbm     = pro_cbm != '' ? parseFloat( pro_cbm ) : 0;

        var cont20 = 0, cont40std = 0, cont40hc = 0, pro_cft = 0;
        if(pro_cbm > 0) {
            cont20      = 20 / pro_cbm;
            cont40std   = 55 / pro_cbm;
            cont40hc    = 67 / pro_cbm;
            pro_cft     = pro_cbm * 35.3147;
        }
        $('#20Cont').val( cont20.toFixed(2) );
        $('#40StdCont').val( cont40std.toFixed(2) );
        $('#40HCCont').val( cont40hc.toFixed(2) );
        $('#productCft').val( pro_cft.toFixed(4) );
    });

    $(document).on('click', '.product_type', function(e) {
        var ptype = $(".product_type:checked").val();

        if(ptype == "K/D") {
            $('.upload_pdf').show();
        } else {
            $('.upload_pdf').hide();
        }
    });

    $(document).on('keyup blur', '#proLcm', function(e) {
        var l_cm = $(this).val();
        l_cm = l_cm != '' ? parseFloat( l_cm ) : 0;
        $("#proLinch").val( (l_cm / 2.54).toFixed(2) );

        var box_l_cm = $("#boxLcm").val();

        $("#boxLcm").val( l_cm + 5 ).trigger('keyup');

        calc_cbm();
    });
    $(document).on('keyup blur', '#proWcm', function(e) {
        var w_cm = $(this).val();
        w_cm = w_cm != '' ? parseFloat( w_cm ) : 0;
        $("#proWinch").val( (w_cm / 2.54).toFixed(2) );

        var box_w_cm = $("#boxWcm").val();

        $("#boxWcm").val( w_cm + 5 ).trigger('keyup');

        calc_cbm();
    });
    $(document).on('keyup blur', '#proHcm', function(e) {
        var h_cm = $(this).val();
        h_cm = h_cm != '' ? parseFloat( h_cm ) : 0;
        $("#proHinch").val( (h_cm / 2.54).toFixed(2) );

        var box_h_cm = $("#boxHcm").val();

        $("#boxHcm").val( h_cm + 5 ).trigger('keyup');

        calc_cbm();

    });

    $(document).on('keyup blur', '#boxLcm', function(e) {
        var l_cm = $(this).val();
        l_cm = l_cm != '' ? parseFloat( l_cm ) : 0;
        $("#boxLinch").val( (l_cm / 2.54).toFixed(2) );
    });
    $(document).on('keyup blur', '#boxWcm', function(e) {
        var w_cm = $(this).val();
        w_cm = w_cm != '' ? parseFloat( w_cm ) : 0;
        $("#boxWinch").val( (w_cm / 2.54).toFixed(2) );
    });
    $(document).on('keyup blur', '#boxHcm', function(e) {
        var h_cm = $(this).val();
        h_cm = h_cm != '' ? parseFloat( h_cm ) : 0;
        $("#boxHinch").val( (h_cm / 2.54).toFixed(2) );
    });

    $(document).on('keyup blur', '.l_cm_dim', function(e) {
        var cm      = $(this).val(),
            target  = $(this).closest('.row').closest('.form-group').parent().next().find('.l_inch_dim');
        cm = cm != '' ? parseFloat( cm ) : 0;
        $(target).val( (cm / 2.54).toFixed(2) );
        console.log(target.val());
    });
    $(document).on('keyup blur', '.w_cm_dim', function(e) {
        var cm      = $(this).val(),
            target  = $(this).closest('.row').closest('.form-group').parent().next().find('.w_inch_dim');
        cm = cm != '' ? parseFloat( cm ) : 0;
        $(target).val( (cm / 2.54).toFixed(2) );
    });
    $(document).on('keyup blur', '.h_cm_dim', function(e) {
        var cm      = $(this).val(),
            target  = $(this).closest('.row').closest('.form-group').parent().next().find('.h_inch_dim');
        cm = cm != '' ? parseFloat( cm ) : 0;
        $(target).val( (cm / 2.54).toFixed(2) );
    });

    $(document).on('keyup blur', '.gw_kg', function(e) {
        var w_kg = $(this).val();
        w_kg = w_kg != '' ? parseFloat( w_kg ) : 0;

        $(this).closest('.row').find('.gw_lbs').val( (w_kg * 2.2046226218).toFixed(2) );

        calcGrossWeight();
    });

    $(document).on('keyup blur', '.gw_lbs', function(e) {
        var w_lbs = $(this).val();
        w_lbs = w_lbs != '' ? parseFloat( w_lbs ) : 0;

        $(this).closest('.row').find('.gw_kg').val( (w_lbs / 2.2046226218).toFixed(2) );

        calcGrossWeight();
    });

    $(document).on('click', '.packing_inp', function(e) {
        var checked = $(this).prop('checked'),
            target  = $(this).closest('.form-group').find('input.packing_value');

        if(checked) {
            target.removeAttr('readonly');
        } else {
            target.attr('readonly', 'readonly');
        }
    });

    $(document).on('keypress', '.bootstrap-tagsinput input', function (e) {
        console.log(e.keyCode);
        if (e.keyCode == 13) {
            e.keyCode = 188;
            e.preventDefault();
        }
    });

    $(document).on('click', '.product-thumbnail img', function(e) {
        var src = $(this).attr('src');

        $("#mainImage").attr('src', src);
    });

    $('.taginput').tagsinput({
        confirmKeys: [13, 188]
    });

    $(document).on('click', 'a[href="#gen_code"]', function(e) {
        e.preventDefault();
        var text = generateRandomString(8);

        $(this).closest('.form-group').find('input').val( text );
    });

    $(document).on('keyup blur', '#product_mp', function() {
        var mp = $(this).val();

        var html = '';

        for(var i = 1; i <= mp; i++) {
            html += '<div>';
            if(i == 1)
                html += '<label class="text-center d-block"><strong>Master Packet (MP) = </strong>'+mp+'</label>';
            else
                html += '<label class="text-center d-block">+</label>';

            html += '<div class="row">';
            html += '<div class="col-sm-6">';
            html += '<div class="form-group">';
            html += '<div class="input-group">';
            html += '<input type="text" name="record[product_gross_weight_kg][]" class="form-control text-center gw_kg" value="" autocomplete="off">';
            html += '<div class="input-group-append">';
            html += '<span class="input-group-text">Kg</span>'
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '<div class="col-sm-6">';
            html += '<div class="form-group">';
            html += '<div class="input-group">';
            html += '<input type="text" name="record[product_gross_weight_lbs][]" class="form-control text-center gw_lbs" autocomplete="off">';
            html += '<div class="input-group-append">';
            html += '<span class="input-group-text">LBS</span>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
        }

        $("#total_gross_weight").html( html );

    });

    $(document).on('keyup blur', '#product_ip', function() {
        var ip = $(this).val();

        var html = '';

        for(var i = 1; i <= ip; i++) {
            html += '<div>';
            if(i == 1)
                html += '<label class="text-center d-block"><strong>Inner Packet (IP) = </strong>'+ip+'</label>';
            else
                html += '<label class="text-center d-block">+</label>';

            html += '<div class="">';
            html += '<div class="form-group">';
            html += '<label>Carton Dimension (In CMS)</label>';

            html += '<div class="row">';
            html += '<div class="col-sm-4">';
            html += '<div class="form-group">';
            html += '<div class="input-group">';
            html += '<div class="input-group-prepend">';
            html += '<span class="input-group-text">L</span>';
            html += '</div>';
            html += '<input type="text" name="record[product_carton_l_cm][]" class="form-control text-center l_cm_dim" value="0" autocomplete="off">';
            html += '</div>';
            html += '</div>';
            html += '</div>';

            html += '<div class="col-sm-4">';
            html += '<div class="form-group">';
            html += '<div class="input-group">';
            html += '<div class="input-group-prepend">';
            html += '<span class="input-group-text">W</span>';
            html += '</div>';
            html += '<input type="text" name="record[product_carton_w_cm][]" class="form-control text-center w_cm_dim" value="0" autocomplete="off">';
            html += '</div>';
            html += '</div>';
            html += '</div>';


            html += '<div class="col-sm-4">';
            html += '<div class="form-group">';
            html += '<div class="input-group">';
            html += '<div class="input-group-prepend">';
            html += '<span class="input-group-text">H</span>';
            html += '</div>';
            html += '<input type="text" name="record[product_carton_h_cm][]" class="form-control text-center h_cm_dim" value="0" autocomplete="off">';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '<div class="">';
            html += '<div class="form-group">';
            html += '<label>Carton Dimension (In Inch)</label>';

            html += '<div class="row">';
            html += '<div class="col-sm-4">';
            html += '<div class="form-group">';
            html += '<div class="input-group">';
            html += '<div class="input-group-prepend">';
            html += '<span class="input-group-text">L</span>';
            html += '</div>';
            html += '<input type="text" name="record[product_carton_l_inch][]" class="form-control text-center l_inch_dim" value="0" autocomplete="off">';
            html += '</div>';
            html += '</div>';
            html += '</div>';

            html += '<div class="col-sm-4">';
            html += '<div class="form-group">';
            html += '<div class="input-group">';
            html += '<div class="input-group-prepend">';
            html += '<span class="input-group-text">W</span>';
            html += '</div>';
            html += '<input type="text" name="record[product_carton_w_inch][]" class="form-control text-center w_inch_dim" value="0" autocomplete="off">';
            html += '</div>';
            html += '</div>';
            html += '</div>';


            html += '<div class="col-sm-4">';
            html += '<div class="form-group">';
            html += '<div class="input-group">';
            html += '<div class="input-group-prepend">';
            html += '<span class="input-group-text">H</span>';
            html += '</div>';
            html += '<input type="text" name="record[product_carton_h_inch][]" class="form-control text-center h_inch_dim" value="0" autocomplete="off">';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
        }

        $("#total_ip_dimension").html( html );

    });

    $(document).on('click', '.product_type', function(e) {
        var ptype = $(".product_type:checked").val();

        if(ptype == "K/D") {
            $('.upload_pdf').show();
        } else {
            $('.upload_pdf').hide();
        }
    });

    $(document).on('click', '.packing_inp', function(e) {
        var checked = $(this).prop('checked'),
            target  = $(this).closest('.form-group').find('input.packing_value');

        if(checked) {
            target.removeAttr('readonly');
        } else {
            target.attr('readonly', 'readonly');
        }
    });

    $(".upload_image input[type=file]").change(function() {
        var target = $(this).closest(".upload_image").find("img");
        readURL(this, target);
    });
});

function calc_cbm() {
    var l = $("#proLcm").val(),
        w = $("#proWcm").val(),
        h = $("#proHcm").val(),
        cbm = 0;

    cbm = (l * w * h) / 1000000;

    $("#productCbm").val(cbm).trigger('keyup');
}
function readURL(input, target) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $(target).attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    } else {
        return;
    }
}

function calcGrossWeight() {
    var totWeightKg = 0, totWeightLbs = 0;
    $(".gw_kg").each(function(){
        totWeightKg     += +$(this).val();
    });
    $(".gw_lbs").each(function(){
        totWeightLbs    += +$(this).val();
    });

    $("#totGrossWeightKg").val( totWeightKg.toFixed(2) );
    $("#totGrossWeightLbs").val( totWeightLbs.toFixed(2) );
}

function calcNetWeight() {
    var wood_kg     = $("#netWgWoodKg").val(),
        wood_lbs    = $("#netWgWoodLbs").val(),
        iron_kg     = $("#netWgIronKg").val(),
        iron_lbs    = $("#netWgIronLbs").val(),
        other_kg    = $("#netWgOtherKg").val(),
        other_lbs   = $("#netWgOtherLbs").val();

    wood_kg     = wood_kg != ''     ? parseFloat(wood_kg)   : 0;
    wood_lbs    = wood_lbs != ''    ? parseFloat(wood_lbs)  : 0;
    iron_kg     = iron_kg != ''     ? parseFloat(iron_kg)   : 0;
    iron_lbs    = iron_lbs != ''    ? parseFloat(iron_lbs)  : 0;
    other_kg     = other_kg != ''   ? parseFloat(other_kg)  : 0;
    other_lbs    = other_lbs != ''  ? parseFloat(other_lbs) : 0;

    $("#netWeightKg").val( wood_kg + iron_kg + other_kg );
    $("#netWeightLbs").val( wood_lbs + iron_lbs + other_lbs );
}

function generateRandomString(length) {
    var text = "";
    var possible = "ABC6DEF4GH1I2J3KL5MN7OP9QRS8TUV1WXYZ";

    for (var i = 0; i < length; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

$(".checkall").on("click", function() {
    var form = $(this).closest("form");

    form.find(".check:not([disabled])").prop( "checked", $(this).prop("checked") );
});

$(".icon-trash-o").on("click", function (e) {
    e.preventDefault();

    var form = $(this).closest("form");
    if(form.find(".check:checked").length > 0) {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover record(s)!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    form.trigger("submit");
                }
            });
    } else {
        swal("Warning", "Select at least one record to delete", "warning");
    }

});
