<?php
Route::get('{company}/user/logout', 'backend\UserController@logout');

Route::get('{company}', 'backend\Dashboard@index');

Route::any('{company}/page/edit/{slug?}', 'backend\Page@edit');

// User
Route::any('{company}/user/add/{role?}/{id?}', 'backend\User@add');
Route::any('{company}/user/{role?}', 'backend\User@index');
Route::any('{company}/change-password', 'backend\User@change_password');

// Account
Route::any('{company}/account/{role}', 'backend\AccountController@index');
Route::any('{company}/account/{role}/add/{id?}', 'backend\AccountController@add');

// Vehicle
Route::any('{company}/vehicle', 'backend\VehicleController@index');
Route::any('{company}/vehicle/add/{id?}', 'backend\VehicleController@add');

// Add-on Items
Route::any('{company}/unit', 'backend\UnitController@index');
Route::any('{company}/add-on-item', 'backend\ItemController@index');
Route::any('{company}/add-on-item/add/{id?}', 'backend\ItemController@add');

// Activity
Route::any('{company}/activity', 'backend\ActivityController@index');
Route::any('{company}/activity/add/{id?}', 'backend\ActivityController@add');

// Leads
Route::any('{company}/leads', 'backend\LeadController@index');
Route::any('{company}/leads/add/{id?}', 'backend\LeadController@add');
Route::any('{company}/leads/follow-up/{type}', 'backend\LeadController@follow_up');
Route::any('{company}/leads/management', 'backend\LeadController@management');

// Visa
Route::any('{company}/visa', 'backend\VisaController@index');
Route::any('{company}/visa/add/{id?}', 'backend\VisaController@add');
Route::any('{company}/visa/application/{id?}', 'backend\VisaController@application');
Route::any('{company}/visa/status/{id}/{status}', 'backend\VisaController@status');

// Location
Route::any('{company}/location/countries/{slug?}', 'backend\Location@country');
Route::post('{company}/location/country/import', 'backend\Location@import_country');
Route::get('{company}/location/country/export', 'backend\Location@export_country');

Route::any('{company}/location/states/{slug?}', 'backend\Location@states');
Route::post('{company}/location/state/import', 'backend\Location@import_state');
Route::get('{company}/location/state/export', 'backend\Location@export_state');

Route::any('{company}/location/cities/{slug?}', 'backend\Location@cities');
Route::post('{company}/location/city/import', 'backend\Location@import_city');
Route::get('{company}/location/city/export', 'backend\Location@export_city');

Route::any('{company}/location/pincode/{slug?}', 'backend\Location@pincode');
Route::post('{company}/location/pincode/import', 'backend\Location@import_pincode');
Route::get('{company}/location/pincode/export', 'backend\Location@export_pincode');

Route::any('{company}/department/{id?}', 'backend\DepartmentController@index');

Route::any('{company}/create-fixed-tour/{id?}', 'backend\TripController@add');
Route::any('{company}/hotels', 'backend\TripController@select_hotel');

// Profile
Route::any('{company}/edit-profile', 'backend\UserController@edit_profile');
Route::any('{company}/change-password', 'backend\UserController@change_password');

// Settings
Route::any('{company}/general-setting', 'backend\SettingController@index');

Route::any('{company}/ajax/user_login', 'backend\Ajax@user_login');
Route::any('{company}/ajax/{action}', 'backend\Ajax@index');
