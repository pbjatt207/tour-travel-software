<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Country extends Model {
    public      $timestamps     = false;
    protected   $table          = 'countries';
    protected   $primaryKey     = 'country_id';

    public function states() {
        return $this->hasMany('App\Model\State', 'state_country', 'country_id');
    }
    public function cities() {
        return $this->hasMany('App\Model\City', 'city_country', 'country_id');
    }
}
