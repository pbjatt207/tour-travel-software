<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class State extends Model {
    public      $timestamps     = false;
    protected   $table          = 'states';
    protected   $primaryKey     = 'state_id';

    public function country() {
        return $this->hasOne('App\Model\Country', 'country_id', 'state_country');
    }
    public function cities() {
        return $this->hasMany('App\Model\City', 'city_state', 'state_id');
    }
}
