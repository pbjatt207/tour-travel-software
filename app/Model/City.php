<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class City extends Model {
    public      $timestamps     = false;
    protected   $table          = 'cities';
    protected   $primaryKey     = 'city_id';

    public function state() {
        return $this->hasOne('App\Model\State', 'state_id', 'city_state');
    }
    public function country() {
        return $this->hasOne('App\Model\Country', 'country_id', 'city_country');
    }
}
