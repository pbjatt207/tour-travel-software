<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model {
    public      $timestamps     = false;
    protected   $table          = 'vehicles';
    protected   $primaryKey     = 'vehicle_id';
}
