<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class User extends Model {
    public      $timestamps     = false;
    protected   $table          = 'users';
    protected   $primaryKey     = 'user_id';

    public function company() {
        return $this->hasOne('App\Model\Company', 'company_uid', 'user_id');
    }
    public function hotel() {
        return $this->hasOne('App\Model\Hotel', 'hotel_uid', 'user_id');
    }
    public function country() {
        return $this->hasOne('App\Model\Country', 'country_id', 'user_country');
    }
    public function state() {
        return $this->hasOne('App\Model\State', 'state_id', 'user_state');
    }
    public function city() {
        return $this->hasOne('App\Model\City', 'city_id', 'user_city');
    }
    public function pincode() {
        return $this->hasOne('App\Model\Pincode', 'pin_id', 'user_pincode');
    }
}
