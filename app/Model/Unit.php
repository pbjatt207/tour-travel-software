<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model {
    public      $timestamps     = false;
    protected   $table          = 'units';
    protected   $primaryKey     = 'unit_id';
}
