<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LeadFollow extends Model {
    public      $timestamps     = false;
    protected   $table          = 'lead_follow_up';
    protected   $primaryKey     = 'lfu_id';

    public function lead() {
        return $this->hasOne('App\Model\Lead', 'lead_id', 'lfu_lid');
    }
    public function support() {
        return $this->hasOne('App\Model\User', 'user_id', 'lfu_added_by');
    }
}
