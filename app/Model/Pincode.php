<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Pincode extends Model {
    public      $timestamps     = false;
    protected   $table          = 'pincodes';
    protected   $primaryKey     = 'pin_id';

    public function city() {
        return $this->hasOne('App\Model\City', 'city_id', 'pin_city');
    }
    public function state() {
        return $this->hasOne('App\Model\State', 'state_id', 'pin_state');
    }
    public function country() {
        return $this->hasOne('App\Model\Country', 'country_id', 'pin_country');
    }
}
