<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model {
    public      $timestamps     = false;
    protected   $table          = 'activity';
    protected   $primaryKey     = 'activity_id';

}
