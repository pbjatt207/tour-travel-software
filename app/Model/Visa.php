<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Visa extends Model {
    public      $timestamps     = false;
    protected   $table          = 'visa';
    protected   $primaryKey     = 'visa_id';

    public function customer() {
        return $this->hasOne('App\Model\User', 'user_id', 'visa_customer');
    }

    public function country() {
        return $this->hasOne('App\Model\Country', 'country_id', 'visa_destination');
    }
}
