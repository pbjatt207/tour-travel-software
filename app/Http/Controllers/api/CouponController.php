<?php
namespace App\Http\Controllers\api;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Model\UserModel as User;
use App\Model\CouponModel as Coupon;
use App\Model\CartModel as Cart;
use App\Model\OrderModel as Order;
use DB;

class CouponController extends BaseController {
    public function date_diff($now, $your_date) {
        $now       = strtotime($now);
        $your_date = strtotime($your_date);

        $datediff  = $now - $your_date;
        return round($datediff / (60 * 60 * 24));
    }
    public function index(Request $request, $user_id) {
        $carts = Cart::join('services AS s', 'carts.cart_sid', 's.service_id')
                        ->select('carts.*', 's.service_cost')
                        ->where('cart_uid', $user_id)->get();

        $query = Coupon::whereRaw(" DATEDIFF(`coupon_valid_from`, CURDATE()) <= 0 AND ( `coupon_never_end` = 1 OR DATEDIFF(CURDATE(), `coupon_valid_upto`) <=0 )  ")
                    ->orWhereRaw('FIND_IN_SET('.$user_id.', coupon_uids)');

        foreach( $carts as $cart ) {
            $query->orWhere( 'coupon_sid', $cart->cart_sid );
        }
        // echo $query->toSql();
        // die;
        $coupons = $query->get();

        if(!$coupons->isEmpty($coupons)) {
            foreach( $coupons as $k => $c ) {
                $coupons[$k]->coupon_image          = !empty($c->coupon_image) ? url('imgs/coupons/'.$c->coupon_image) : '';
                $coupons[$k]->coupon_description    = preg_split('/\r\n|\r|\n/', $c->coupon_description);
            }
            $re = [
                'status'    => TRUE,
                'data'      => $coupons,
                'message'   => $coupons->count().' record(s) found.'
            ];
        } else {
            $re = [
                'status'    => FALSE,
                'message'   => 'Empty record(s) found.'
            ];
        }

        return response()->json($re);
    }
    public function apply(Request $request) {
        $post = $request->isMethod('post') ? $request->input() : [];
        if( !empty($post['user_id']) && !empty($post['coupon_code']) ) {

            $promo = Coupon::where('coupon_code', $post['coupon_code'])->first();

            if(!empty($promo->coupon_id)) {

                $carts = Cart::join('services AS s', 'carts.cart_sid', 's.service_id')
                                ->select('carts.*', 's.service_cost')
                                ->where('cart_uid', $post['user_id'])
                                ->get();

                $total_uses      = Order::where('order_cid', $promo->coupon_id)->count();
                $total_user_used = Order::where('order_cid', $promo->coupon_id)->where('order_uid', $post['user_id'])->count();

                $sidArr = $ssidArr = [];
                $cartTotal = $discount = 0;
                foreach($carts as $cart) {
                    $sidArr[]  = $cart->cart_sid;
                    // $sidArr[]   = $cart->subservice_sid;
                    $cartTotal += $cart->service_cost;
                }
                $sidArr = array_unique( $sidArr );

                $current_date       = date('Y-m-d', time());
                $coupon_start_date  = $promo->coupon_valid_from;


                $isExpired     = 0;
                if($promo->coupon_never_end == 0) {
                    $coupon_end_date    = $promo->coupon_valid_upto;
                    $isExpired = ($this->date_diff( $coupon_end_date, $current_date ) <= 0);
                }

                $date_diff      = $this->date_diff( $current_date, $coupon_start_date );
                $isStarted      = ($date_diff >= 0);

                $uidArr         = !empty($promo->coupon_uids) ? explode(",", $promo->coupon_uids) : [ $post['user_id'] ];

                if(!$isExpired && $isStarted && in_array( $post['user_id'], $uidArr )) {
                    if(!empty($promo->coupon_sid) && !in_array($promo->coupon_sid, $sidArr)) {
                        $re = [
                            'status'        => FALSE,
                            'explaination'  => 'Service mismatched.',
                            'message'       => 'Coupon code is not valid.'
                        ];
                    } elseif(!empty($promo->coupon_ssid) && !in_array($promo->coupon_ssid, $ssidArr)) {
                        $re = [
                            'status'        => FALSE,
                            'explaination'  => 'Subservice mismatched.',
                            'message'       => 'Coupon code is not valid.'
                        ];
                    } elseif($promo->coupon_is_total_cart && $promo->coupon_min_cart_total > $cartTotal) {
                        $re = [
                            'status'    => FALSE,
                            'message'   => 'Coupon code is not valid, must have min. cart total '.$promo->coupon_min_cart_total
                        ];
                    } elseif($promo->coupon_is_user_limit && $promo->coupon_limit_no <= $total_uses) {
                        $re = [
                            'status'        => FALSE,
                            'explaination'  => 'User limit exists. Limit is '.$promo->coupon_is_user_limit.' / '.$total_uses,
                            'message'       => 'Coupon code has expired.'
                        ];
                    } elseif($promo->coupon_is_total_uses && $promo->coupon_total_uses <= $total_user_used) {
                        $re = [
                            'status'        => FALSE,
                            'explaination'  => 'User per person limit exists. Limit is '.$promo->coupon_total_uses.' / '.$total_user_used,
                            'message'       => 'Coupon code has expired.'
                        ];
                    } else {
                        $discount = 0;
                        if(!empty($promo->coupon_sid)) {
                            foreach($carts as $cart) {
                                if(!empty($promo->coupon_sid) && $cart->subservice_sid == $promo->coupon_sid) {
                                    if(empty($promo->coupon_ssid) || $cart->cart_sid == $promo->coupon_ssid) {
                                        $discount += $promo->coupon_discount_type == 'Percent' ? $cart->subservice_price * $promo->coupon_discount / 100 : $promo->coupon_discount;
                                    }
                                }
                            }
                        } else {
                            $discount = $promo->coupon_discount_type == 'Percent' ? $cartTotal * $promo->coupon_discount / 100 : $promo->coupon_discount;
                        }

                        if($discount <= $cartTotal) {
                            $re = [
                                'status'        => TRUE,
                                'coupon_info'   => $promo,
                                'coupon_type'   => $promo->coupon_type,
                                'discount'      => $discount,
                                'total_cart'    => $cartTotal,
                                'message'       => 'Coupon code '.$post['coupon_code'].' applied.'
                            ];
                        } else {
                            $re = [
                                'status'        => FALSE,
                                'explaination'  => 'Discount is greater than total cart amount.',
                                'message'       => 'Coupon code is not valid.'
                            ];
                        }
                    }
                } elseif($isExpired) {
                    $re = [
                        'status'    => FALSE,
                        'message'   => 'Coupon code has been expired.'
                    ];
                } elseif(!$isStarted) {
                    $re = [
                        'status'        => FALSE,
                        'explaination'  => 'start date is in future.',
                        'message'       => 'Coupon code is not valid.'
                    ];
                } else {
                    $re = [
                        'status'        => FALSE,
                        'explaination'  => 'User specified.',
                        'message'       => 'Coupon code is not valid.'
                    ];
                }
            } else {
                $re = [
                    'status'        => FALSE,
                    'explaination'  => 'Not available',
                    'message'       => 'Coupon code is not valid'
                ];
            }
        } else {
            $re = [
                'status'    => FALSE,
                'message'   => 'Required field(s) missing.'
            ];
        }

        return response()->json($re);
    }
}
