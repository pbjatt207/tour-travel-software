<?php
namespace App\Http\Controllers\api;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Model\ChooseModel as Cmodel;
use App\Model\AddressModel as Addr;
use DB;

class ChooseController extends BaseController {
    public function index() {
        $chooses  = Cmodel::where('choose_is_deleted', 'N')->get();

        if(!$chooses->isEmpty()) {
            foreach ($chooses as $key => $s) {
                if(!empty($s->choose_icon)) {
                    $chooses[$key]->choose_icon = url('imgs/choose/'.$s->choose_icon);
                }
            }
            $re = [
                'status'    => TRUE,
                'message'   => 'Records found.',
                'data'      => $chooses
            ];
        } else {
            $re = [
                'status'    => FALSE,
                'message'   => 'Empty record(s) found.'
            ];
        }

        return response()->json($re);
    }
}
