<?php
namespace App\Http\Controllers\api;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Model\OrderModel as Order;
use App\Model\TimeslotModel AS Tslot;

class TaskController extends BaseController {
    public function index(Request $request, $user_id) {
        if(!empty($user_id)) {
            $orders = Order::where('order_vid', $user_id)->where('order_status', '!=', 'Completed')->orderBy('order_id', 'DESC')->get();
            $today = $tomorrow = $this_week = $pending = $cancelled = $completed = 0;

            $weekDays = [];
            for($i = 1; $i <= 7; $i++) {
                $weekDays[] = date("d M D", strtotime('+'.$i.' days'));
            }

            if(!$orders->isEmpty()) :
                foreach($orders as $o) {
                    if( $o->order_schedule_date == date("d M D", time()) ) {
                        $today++;
                    }
                    if( $o->order_schedule_date == date("d M D", strtotime('+1 days')) ) {
                        $tomorrow++;
                    }
                    if( in_array($o->order_schedule_date, $weekDays) ) {
                        $this_week++;
                    }
                    if( $o->order_status == "Pending" ) {
                        $pending++;
                    }
                    if( $o->order_status == "Cancelled" ) {
                        $cancelled++;
                    }
                    if( $o->order_status == "Completed" ) {
                        $completed++;
                    }
                }
            endif;

            $status = TRUE;
            $re = compact('status', 'today', 'tomorrow', 'this_week', 'pending', 'cancelled', 'completed');
        } else {
            $re = [
                'status'    => FALSE,
                'message'   => 'Required field(s) missing.'
            ];
        }

        return response()->json($re);
    }

    public function oneday_task(Request $request, $type, $user_id) {
        $date = $type == "today" ? date("d M D", time()) : date("d M D", strtotime("+1 day"));

        $tasks = Order::join('order_products AS op', 'orders.order_id', 'opro_oid')
                    ->leftJoin('user_addresses AS addr', 'orders.order_address', 'addr.uaddr_id')
                    ->leftJoin('users AS u', 'orders.order_uid', 'u.user_id')
                    ->orderBy('order_id', 'DESC')
                    ->where('order_vid', $user_id)
                    ->where("order_schedule_date", $date)
                    ->where('order_status', '!=', 'Completed')
                    ->get();

        $date = $type == "today" ? date("d F Y", time()) : date("d F Y", strtotime("+1 day"));
        $day  = $type == "today" ? date("l", time()) : date("l", strtotime("+1 day"));


        $timeArr = $timeArr1 = $taskArr = [];

        if(!$tasks->isEmpty()) {
            foreach ($tasks as $key => $t) {
                $timeArr1[] = $t->order_schedule_time;
            }
            $timeArr1 = array_unique( $timeArr1 );

            $tslots = Tslot::orderBy('tslot_min_time')->get();
            foreach ($tslots as $ts) {
                $timeArr[] = $ts->tslot_name;
            }

            foreach($timeArr as $key => $t) {
                if(!in_array($t, $timeArr1)) {
                    unset($timeArr[$key]);
                }
            }
            $timeArr = array_filter( $timeArr );

            foreach($timeArr as $time) {
                $t_arr = [];
                foreach ($tasks as $key => $t) {
                    if($time == $t->order_schedule_time) {
                        $t_arr[] = $t;
                    }
                }
                $taskArr[] = [
                    'time'      => $time,
                    'tasks'     => $t_arr
                ];
            }

            $re = [
                'status'    => TRUE,
                'message'   => $tasks->count()." record(s) found.",
                'data'      => $taskArr,
                'date'      => $date,
                'day'       => $day
            ];
        } else {
            $re = [
                'status'    => FALSE,
                'message'   => 'Tasks not found.',
                'date'      => $date,
                'day'       => $day
            ];
        }

        return response()->json($re);
    }

    public function tasks(Request $request, $type, $user_id) {
        if(!empty($user_id)) {
            $today  = $tomorrow = $this_week = $pending = $cancelled = $completed = 0;

            $weekDays = [];
            for($i = 1; $i <= 7; $i++) {
                $weekDays[] = date("d M D", strtotime('+'.$i.' days'));
            }

            $query = Order::join('order_products AS op', 'orders.order_id', 'opro_oid')
                    ->leftJoin('user_addresses AS addr', 'orders.order_address', 'addr.uaddr_id')
                    ->leftJoin('users AS u', 'orders.order_uid', 'u.user_id')
                    ->where('order_vid', $user_id);

            if($type == "next_week") {
                $query->whereIn('order_schedule_date', $weekDays);
            } elseif($type == "pending") {
                $query->where('order_status', 'Pending');
            }

            $orders = $query->orderBy('order_id', 'DESC')->get();

            // $records = [];
            // foreach($orders as $o) {
            //     if($type == "today" ) {
            //
            //     }
            // }

            if(!$orders->isEmpty()) :
                // if($type == "next_week") {
                $dateArr = $taskArr = [];
                foreach($orders as $o) {
                    $dateArr[] = $o->order_schedule_date;
                }
                $dateArr = array_unique($dateArr);
                foreach($dateArr as $dt) {
                    $t_arr = [];
                    foreach($orders as $o) {
                        if($o->order_schedule_date == $dt) {
                            $t_arr[] = $o;
                        }
                    }
                    $taskArr[] = [
                        'date'  => $dt,
                        'tasks' => $t_arr
                    ];
                }
                // } else {
                //     $taskArr = $orders;
                // }

                $re = [
                    'status'    => TRUE,
                    'message'   => $orders->count()." record(s) found.",
                    'data'      => $taskArr
                ];
            else:
                $re = [
                    'status'    => FALSE,
                    'message'   => 'No record(s) found.'
                ];
            endif;
        } else {
            $re = [
                'status'    => FALSE,
                'message'   => 'Required field(s) missing.'
            ];
        }

        return response()->json($re);
    }
}
