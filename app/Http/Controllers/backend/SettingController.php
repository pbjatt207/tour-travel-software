<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;

use App\Http\Controllers\BaseController;
use App\Model\Company;

class SettingController extends BaseController {
    public function index(Request $request, $company) {
        $profile  = $this->profile($request);

        if($request->isMethod('post')) {
            $input = $request->company;
            Company::where('company_sitename', $company)->update($input);

            if ($request->hasFile('company_logo')) {
                if(!empty($profile->company->company_logo) && file_exists(public_path()."/imgs/companies/".$profile->company->company_logo)) {
                    unlink(public_path()."/imgs/companies/".$profile->company->company_logo);
                }

                $image           = $request->file('company_logo');
                $name            = 'logo_'.$id.'.'.$image->getClientOriginalExtension();
                $destinationPath = "public/imgs/companies/";
                $image->move($destinationPath, $name);

                if(!empty($profile->company->company_logo)) {
                    $name .= "?v=".uniqid();
                }

                User::where('user_id', $id)->update( array('company_logo' => $name) );
            }

            if ($request->hasFile('company_favicon')) {
                if(!empty($profile->company->company_favicon) && file_exists(public_path()."/imgs/companies/".$profile->company->company_favicon)) {
                    unlink(public_path()."/imgs/companies/".$profile->company->company_favicon);
                }

                $image           = $request->file('company_favicon');
                $name            = 'logo_'.$id.'.'.$image->getClientOriginalExtension();
                $destinationPath = "public/imgs/companies/";
                $image->move($destinationPath, $name);

                if(!empty($profile->company->company_favicon)) {
                    $name .= "?v=".uniqid();
                }

                User::where('user_id', $id)->update( array('company_favicon' => $name) );
            }

            return redirect()->back()->with('success', 'information updated');
        }

        $title 	= "General Settings";
        $page 	= "general_settings";
        $data 	= compact('page', 'title');
        return view('backend/layout', $data);
    }
}
