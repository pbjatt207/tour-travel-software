<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Model\Unit;

class UnitController extends BaseController {
    public function index(Request $request, $company, $id = null) {
        $profile = $this->profile($request);

    	$edit = [];
    	if(!empty($id)) {
    		$edit = Unit::find($id);
    	}

    	if($request->isMethod('post')) {
    		$input = $request->input('record');
    		if(!empty($input)) {

                $isExistsQuery = Unit::where('unit_name', 'LIKE', $input['unit_name'])->where('unit_added_by', $profile->user_id);

                if(!empty($edit->unit_id)) {
                    $isExistsQuery->where('unit_id', '!=', $edit->unit_id);
                }

                $isExists = $isExistsQuery->count();

                if(!$isExists) :
                    $input['unit_added_by'] = $profile->user_id;

    	    		if(empty($edit->unit_id)) {
    		    		$id   = Unit::insertGetId($input);
                        $mess = "A new record has been added.";
    		    	} else {
    		    		Unit::where('unit_id', $id)->update($input);
                        $mess = "A record has been updated.";
    		    	}
                else :
                    return redirect()->back()->with('danger', 'Department name already exists.');
                endif;
		    }

		    $check = $request->input('check');
		    if(!empty($check)) {
		    	Unit::whereIn('unit_id', $check)->delete();
                $mess = "Selected record(s) has been removed.";
		    }

	    	return redirect()->back()->with('success', $mess);
    	}
        // \DB::enableQueryLog();
        $query   = Unit::where('unit_added_by', $profile->user_id);
        $search  = $request->input('search');
        if(!empty($search['keyword'])) {
            $query->whereRaw("LOWER(unit_name) LIKE '%".trim(strtolower($search['keyword']))."%'");
        }
    	$records = $query->orderBy('unit_id', 'DESC')->paginate(30);
        // dd(\DB::getQueryLog());

        $title 	= "Units";
        $page 	= "unit";
        $data 	= compact('page', 'title', 'records', 'edit', 'search');
        return view('backend/layout', $data);
    }
}
