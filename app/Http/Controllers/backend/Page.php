<?php

namespace App\Http\Controllers\backend;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;
use App\Model\PageModel AS Pmodel;

class Page extends BaseController {
    public function edit(Request $request, $slug = null) {
        $edit = [];
        if(!empty($slug)) {
            $edit       = Pmodel::where('page_slug', $slug)->first();
        }

        $title 	    = "Edit Page - ".$edit->page_title;
        $page 	    = "edit_page";

        if($request->isMethod('post')) {
           $record   = $request->input('record');

           Pmodel::where('page_slug', $slug)->update($record);

           return redirect('service-panel/page/edit/'.$slug)->with('success', "Success! record update.");
        }

        $data 	= compact('page', 'title', 'edit');
        return view('backend/layout', $data);
    }
}
