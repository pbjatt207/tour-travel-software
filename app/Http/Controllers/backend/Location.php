<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Model\Country;
use App\Model\State;
use App\Model\City;
use App\Model\Pincode;

class Location extends BaseController {
    public function country(Request $request, $company, $id = null) {
        $profile = $this->profile($request);
    	$edit = [];
    	if(!empty($id)) {
    		$edit = Country::find($id);
    	}

    	if($request->isMethod('post')) {
    		$input = $request->input('record');
    		if(!empty($input)) {

                $isExistsQuery = Country::where(function($q) use ($input) {
                    $q->where('country_name', $input['country_name'])
                      ->orWhere('country_code', $input['country_code']);
                })->where('country_is_deleted', 'N');

                if(!empty($edit->country_id)) {
                    $isExistsQuery->where('country_id', '!=', $edit->country_id);
                }

                $isExists = $isExistsQuery->count();

                if(!$isExists) :
    	    		if(empty($edit->country_id)) {
    		    		$id   = Country::insertGetId($input);
                        $mess = "A new record has been added.";
    		    	} else {
    		    		Country::where('country_id', $id)->update($input);
                        $mess = "A record has been updated.";
    		    	}
                else :
                    return redirect($company.'/location/countries')->with('danger', 'Country name or code already exists.');
                endif;
		    }

		    $check = $request->input('check');
		    if(!empty($check)) {
		    	Country::whereIn('country_id', $check)->update(['country_is_deleted' => 'Y']);
                $mess = "A record has been removed.";
		    }

	    	return redirect($company.'/location/countries')->with('success', $mess);
    	}

        $query   = Country::withCount(['states', 'cities'])->where('country_is_deleted', 'N');
        $search  = $request->input('search');
        if(!empty($search['keyword'])) {
            $query->where(function($q) use($search) {
                $q->where('country_name', 'LIKE', '%'.$search['keyword'].'%')
                  ->orWhere('country_short_name', 'LIKE', '%'.$search['keyword'].'%')
                  ->orWhere('country_code', 'LIKE', '%'.$search['keyword'].'%');
            });
        }

    	$records = $query->orderBy('country_name', 'ASC')->paginate(30);

        $title 	= "Countries";
        $page 	= "country";
        $data 	= compact('page', 'title', 'records', 'edit', 'search');
        return view('backend/layout', $data);
    }
    public function export_country(Request $request, $company) {
        $records = Country::where('country_is_deleted', 'N')->get()->toArray();

        $fields = $values = [];
        foreach($records as $i => $rec) {
            if($i == 0) {
                $fields = array_keys($rec);
            }
            $values[] = array_values($rec);
        }

        $filename = "CountriesData_{$company}.csv";

        // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");

        ob_start();
        $df = fopen("php://output", 'w');
        fputcsv($df, $fields);
        foreach($values as $row) {
            fputcsv($df, $row);
        }
        fclose($df);
        return ob_get_clean();
    }
    public function import_country(Request $request, $company) {
        $file = $request->file('import_file');

        $fileName = $file->getPathName();

        $file = fopen($fileName, "r");
        $i = 0;
        while (($getData = fgetcsv($file, 10000, ",")) !== FALSE) {

            if($i == 0) {
                $fieldArr = $getData;
            } else {
                $row = array_combine( $fieldArr, $getData );

                Country::insert( $row );
            }

            $i++;
        }

        return redirect( url( $company.'/location/countries' ) )->with('success', 'Data has been imported.');
    }
    public function states(Request $request, $company, $id = null) {
        $edit = [];
        if(!empty($id)) {
            $edit = State::find($id);
        }

        if($request->isMethod('post')) {
            $input = $request->input('record');

            if(!empty($input)) {
                $isExistsQuery = State::where('state_name', $input['state_name'])->where('state_country', $input['state_country'])->where('state_is_deleted', 'N');

                if(!empty($edit->state_id)) {
                    $isExistsQuery->where('state_id', '!=', $edit->state_id);
                }

                $isExists = $isExistsQuery->count();

                if(!$isExists) :

                    if(empty($edit->state_id)) {
                        $id = State::insertGetId($input);
                        $mess = "A new record has been added.";
                    } else {
                        State::where('state_id', $id)->update($input);
                        $mess = "A record has been updated.";
                    }

                else :
                    return redirect()->back()->with('danger', 'State name in selected country is already exists.');
                endif;
            }

            $check = $request->input('check');
            if(!empty($check)) {
                State::whereIn('state_id', $check)->update(['state_is_deleted' => 'Y']);
                $mess = "A new record has been removed.";
            }

            return redirect($company.'/location/states')->with('success', $mess);
        }


        $countries  = Country::where('country_is_deleted', 'N')->get();
        $query      = State::with(['country'])->withCount(['cities'])->where('state_is_deleted', 'N');

        $search     = $request->input('search');
        if(!empty($search['country'])) {
            $query->where('state_country', $search['country']);
        }
        if(!empty($search['keyword'])) {
            $query->where(function($q) use($search) {
                $q->where('state_name', 'LIKE', '%'.$search['keyword'].'%')
                  ->orWhere('state_short_name', 'LIKE', '%'.$search['keyword'].'%');
            });
        }

        $records    = $query->paginate(10);

        $title  = "States";
        $page   = "state";
        $data   = compact('page', 'title', 'records', 'edit', 'countries', 'search');
        return view('backend/layout', $data);
    }
    public function export_state(Request $request, $company) {
        $records = State::join('countries AS c', 'states.state_country', 'c.country_id')->where('country_is_deleted', 'N')->select('states.*', 'c.country_name')->get()->toArray();

        $fields = $values = [];
        foreach($records as $i => $rec) {
            if($i == 0) {
                $fields = array_keys($rec);
            }
            $values[] = array_values($rec);
        }

        $filename = "StatesData_{$company}.csv";

        // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");

        ob_start();
        $df = fopen("php://output", 'w');
        fputcsv($df, $fields);
        foreach($values as $row) {
            fputcsv($df, $row);
        }
        fclose($df);
        return ob_get_clean();
    }
    public function import_state(Request $request, $company) {
        $file = $request->file('import_file');

        $fileName = $file->getPathName();

        $file = fopen($fileName, "r");
        $i = 0;
        while (($getData = fgetcsv($file, 10000, ",")) !== FALSE) {

            if($i == 0) {
                $fieldArr = $getData;
            } else {
                $row = array_combine( $fieldArr, $getData );
                $country = Country::where('country_name', $row['country_name'])
                            ->where('country_is_deleted', 'N')
                            ->select('country_id')
                            ->first();
                if(!empty($country->country_id)) {
                    $row['state_country'] = $country->country_id;
                } else {
                    $row['state_country'] = Country::insertGetId( ['country_name' => $row['country_name']] );
                }
                unset($row['country_name']);

                State::insert( $row );
            }

            $i++;
        }

        return redirect( url( $company.'/location/states' ) )->with('success', 'Data has been imported.');
    }

    public function cities(Request $request, $company, $id = null) {
        if($request->isMethod('post')) {
            $input = $request->input('record');
            if(!empty($input)) {
                if(empty($id)) {
                    $id = City::insertGetId($input);
                } else {
                    City::where('city_id', $id)->update($input);
                }
            }

            $check = $request->input('check');
            if(!empty($check)) {
                City::whereIn('city_id', $check)->update(['city_is_deleted' => 'Y']);
            }

            return redirect($company.'/location/cities');
        }

        $edit = $states = array();
        $countries  = Country::where('country_is_deleted', 'N')->get();

        if(!empty($id)) {
            $edit = City::where('city_id', $id)->first();
            if(!empty($edit->city_country)) {
                $states = State::where('state_is_deleted', 'N')->where('state_country', $edit->city_country)->get();
            }
        }

        $query      = City::with(['country', 'state'])->where('city_is_deleted', 'N');

        $s_states   = [];
        $search     = $request->input('search');
        if(!empty($search['country'])) {
            $query->where('city_country', $search['country']);
            $s_states = State::where('state_is_deleted', 'N')->where('state_country', $search['country'])->get();
        }
        if(!empty($search['state'])) {
            $query->where('city_state', $search['state']);
        }
        if(!empty($search['keyword'])) {
            $query->where(function($q) use($search) {
                $q->where('city_name', 'LIKE', '%'.$search['keyword'].'%')
                  ->orWhere('city_short_name', 'LIKE', '%'.$search['keyword'].'%');
            });
        }

        $records    = $query->paginate(10);

        $title  = "Cities";
        $page   = "city";
        $data   = compact('page', 'title', 'records', 'edit', 'countries', 'states', 's_states', 'search');
        return view('backend/layout', $data);
    }
    public function export_city(Request $request, $company) {
        $records = City::join('states AS s', 'cities.city_state', 's.state_id')->join('countries AS c', 'cities.city_country', 'c.country_id')->where('country_is_deleted', 'N')->select('cities.*', 's.state_name', 'c.country_name')->get()->toArray();

        $fields = $values = [];
        foreach($records as $i => $rec) {
            if($i == 0) {
                $fields = array_keys($rec);
            }
            $values[] = array_values($rec);
        }

        $filename = "CitiesData_{$company}.csv";

        // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");

        ob_start();
        $df = fopen("php://output", 'w');
        fputcsv($df, $fields);
        foreach($values as $row) {
            fputcsv($df, $row);
        }
        fclose($df);
        return ob_get_clean();
    }
    public function import_city(Request $request, $company) {
        $file = $request->file('import_file');

        $fileName = $file->getPathName();

        $file = fopen($fileName, "r");
        $i = 0;
        while (($getData = fgetcsv($file, 10000, ",")) !== FALSE) {

            if($i == 0) {
                $fieldArr = $getData;
            } else {
                $row = array_combine( $fieldArr, $getData );
                $country = Country::where('country_name', $row['country_name'])
                            ->where('country_is_deleted', 'N')
                            ->select('country_id')
                            ->first();
                if(!empty($country->country_id)) {
                    $row['city_country'] = $country->country_id;
                } else {
                    $row['city_country'] = Country::insertGetId( ['country_name' => $row['country_name']] );
                }
                $state = State::where('state_name', $row['state_name'])
                        ->where('state_country', $row['city_country'])
                        ->where('state_is_deleted', 'N')
                        ->select('state_id')
                        ->first();

                if(!empty($state->state_id)) {
                    $row['city_state'] = $state->state_id;
                } else {
                    $row['city_state'] = State::insertGetId( ['state_name' => $row['state_name'], 'state_country' => $row['city_country']] );
                }
                unset($row['country_name']);
                unset($row['state_name']);

                State::insert( $row );
            }

            $i++;
        }

        return redirect( url( $company.'/location/cities' ) )->with('success', 'Data has been imported.');
    }

    public function pincode(Request $request, $company, $id = null) {
        if($request->isMethod('post')) {
            $input = $request->input('record');
            if(!empty($input)) {
                if(empty($id)) {
                    $id = Pincode::insertGetId($input);
                } else {
                    Pincode::where('pin_id', $id)->update($input);
                }
            }

            $check = $request->input('check');
            if(!empty($check)) {
                Pincode::whereIn('pin_id', $check)->update(['pin_is_deleted' => 'Y']);
            }

            return redirect($company.'/location/pincode');
        }

        $edit = $states = $cities = [];
        $countries  = Country::where('country_is_deleted', 'N')->get();

        if(!empty($id)) {
            $edit = Pincode::find($id);
            if(!empty($edit->pin_country)) {
                $states = State::where('state_is_deleted', 'N')->where('state_country', $edit->pin_country)->get();
            }
            if(!empty($edit->pin_state)) {
                $cities = City::where('city_is_deleted', 'N')->where('city_state', $edit->pin_state)->get();
            }
        }

        $query      = Pincode::with(['country', 'state', 'city'])->where('pin_is_deleted', 'N');

        $s_states   = $s_cities = [];
        $search     = $request->input('search');
        if(!empty($search['country'])) {
            $query->where('pin_country', $search['country']);
            $s_states = State::where('state_is_deleted', 'N')->where('state_country', $search['country'])->get();
        }
        if(!empty($search['state'])) {
            $query->where('pin_state', $search['state']);
            $s_cities = City::where('city_is_deleted', 'N')->where('city_state', $search['state'])->get();
        }
        if(!empty($search['city'])) {
            $query->where('pin_city', $search['city']);
        }
        if(!empty($search['keyword'])) {
            $query->where('pin_code', 'LIKE', '%'.$search['keyword'].'%');
        }

        $records    = $query->paginate(10);

        $title  = "Pincodes";
        $page   = "pincode";
        $data   = compact('page', 'title', 'records', 'edit', 'countries', 'states', 'cities', 's_states', 's_cities', 'search');
        return view('backend/layout', $data);
    }
    public function export_pincode(Request $request, $company) {
        $records = Pincode::join('cities AS ct', 'pincodes.pin_city')->join('states AS s', 'pincodes.pin_state', 's.state_id')->join('countries AS c', 'pincodes.pin_country', 'c.country_id')->where('country_is_deleted', 'N')->select('pincodes.*', 'ct.city_name', 's.state_name', 'c.country_name')->get()->toArray();

        $fields = $values = [];
        foreach($records as $i => $rec) {
            if($i == 0) {
                $fields = array_keys($rec);
            }
            $values[] = array_values($rec);
        }

        $filename = "PincodesData_{$company}.csv";

        // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");

        ob_start();
        $df = fopen("php://output", 'w');
        fputcsv($df, $fields);
        foreach($values as $row) {
            fputcsv($df, $row);
        }
        fclose($df);
        return ob_get_clean();
    }
    public function import_pincode(Request $request, $company) {
        $file = $request->file('import_file');

        $fileName = $file->getPathName();

        $file = fopen($fileName, "r");
        $i = 0;
        while (($getData = fgetcsv($file, 10000, ",")) !== FALSE) {

            if($i == 0) {
                $fieldArr = $getData;
            } else {
                $row = array_combine( $fieldArr, $getData );
                $country = Country::where('country_name', $row['country_name'])
                            ->where('country_is_deleted', 'N')
                            ->select('country_id')
                            ->first();
                if(!empty($country->country_id)) {
                    $row['pin_country'] = $country->country_id;
                } else {
                    $row['pin_country'] = Country::insertGetId( ['country_name' => $row['country_name']] );
                }
                $state = State::where('state_name', $row['state_name'])
                        ->where('state_country', $row['city_country'])
                        ->where('state_is_deleted', 'N')
                        ->select('state_id')
                        ->first();

                if(!empty($state->state_id)) {
                    $row['pin_state'] = $state->state_id;
                } else {
                    $row['pin_state'] = State::insertGetId( ['state_name' => $row['state_name'], 'state_country' => $row['pin_country']] );
                }
                $city = City::where('city_name', $row['city_name'])
                        ->where('city_country', $row['pin_country'])
                        ->where('city_is_deleted', 'N')
                        ->select('city_id')
                        ->first();

                if(!empty($state->state_id)) {
                    $row['pin_city'] = $city->city_id;
                } else {
                    $row['pin_city'] = City::insertGetId( ['pin_code' => $row['pin_code'], 'pin_country' => $row['pin_country']] );
                }
                unset($row['country_name']);
                unset($row['state_name']);
                unset($row['city_name']);

                State::insert( $row );
            }

            $i++;
        }

        return redirect( url( $company.'/location/cities' ) )->with('success', 'Data has been imported.');
    }
}
