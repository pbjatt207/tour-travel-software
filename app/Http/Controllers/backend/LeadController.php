<?php
namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

use App\Model\Lead;
use App\Model\LeadFollow;
use App\Model\User;
use App\Model\Role;
use App\Model\Country;
use App\Model\State;
use App\Model\City;
use App\Model\VisaType;

class LeadController extends BaseController {
    public function index(Request $request, $company, $id = NULL) {
        $profile = $this->profile($request);

        $title 	= "Leads";
        $page 	= "view_leads";

        $states = $cities = $pincodes = array();
        $search = $request->input('search');

        $withAction = ['user', 'country', 'state', 'city', 'pincode'];

        $query   =  Lead::with($withAction)->where('lead_is_deleted', 'N')->where('lead_company', $profile->user_company);

        if(!empty($search['keyword'])) {
            $query->where(function($q) use($search) {
                $s = trim( $search['keyword'] );

                $q->where('lead_customer_name', 'LIKE', '%'.$s.'%')
                  ->orWhere('lead_customer_email', 'LIKE', '%'.$s.'%')
                  ->orWhere('lead_contact_no', 'LIKE', '%'.$s.'%');
            });
        }

        $records = $query->orderBy('lead_id', 'DESC')->paginate(30);
        $customers = User::where('user_is_deleted', 'N')->where('user_role', 'customer')->get();

        $data 	= compact('page', 'title', 'records', 'search', 'customers');
        return view('backend/layout', $data);
    }

    public function add(Request $request, $company, $id = null) {
        $profile    = $this->profile($request);

        $country    = Country::where('country_is_deleted', 'N')->where('country_name', 'LIKE', 'India')->first();

        $countries  = Country::where('country_is_deleted', 'N')->orderBy('country_name')->get();

        $country_id = $country->country_id;

        $edit = $states = $cities = $pincodes = array();
        if(!empty($id)) {
            $edit       = Lead::find($id);
        }

        if(!empty($country->country_id)) {
            $states = State::where('state_is_deleted', 'N')->where('state_country', $country->country_id)->orderBy('state_name')->get();
        }

        if(!empty($edit->lead_state)) {
            $cities = City::where('city_is_deleted', 'N')->where('city_state', $edit->lead_state)->orderBy('city_name')->get();
        }

        $title 	= empty($edit->lead_id) ? "Add Lead" : "Edit Lead";
        $page 	= "add_lead";

        if($request->isMethod('post')) {
            $post = $request->input();

            $record = $post['record'];
            $user   = $post['user'];

            $record['lead_updated_on']    = date('Y-m-d H:i:s', time());
            $record['lead_added_by']      = $profile->user_id;
            $record['lead_company']       = $profile->user_company;

            $record = array_filter($record);

            if(empty($id)) {
                if(!empty( $post['create_account'] ) && $post['create_account'] == "Yes") {
                    $nameArr            = explode(" ", $record['lead_customer_name']);
                    $user['user_fname'] = $nameArr[0];

                    unset( $nameArr[0] );
                    $lname   = implode(" ", $nameArr);
                    $user['user_lname'] = $lname;

                    $user['user_name']      = $record['lead_customer_name'];
                    $user['user_email']     = $record['lead_customer_email'];
                    $user['user_mobile']    = $record['lead_contact_no'];
                    $user['user_phone']     = $record['lead_phone'];
                    $user['user_address1']  = $record['lead_address1'];
                    $user['user_country']   = $record['lead_country'];
                    $user['user_state']     = $record['lead_state'];
                    $user['user_city']      = $record['lead_city'];
                    $user['user_pincode']   = $record['lead_pincode'];
                    $user['user_role']      = 'customer';
                    $user['user_created_on']= date('Y-m-d H:i:s', time());
                    $user['user_updated_on']= date('Y-m-d H:i:s', time());
                    $user['user_is_blocked']= 'N';
                    $user['user_company']   = $profile->user_company;
                    $user['user_added_by']  = $profile->user_id;

                    $user_id = User::insertGetId( $user );

                    $record['lead_uid']     = $user_id;
                }

                $record['lead_created_on'] = date('Y-m-d H:i:s', time());
                $id = Lead::insertGetId($record);

                // $arr = [
                //     'lfu_lid'       => $id,
                //     'lfu_message'   => $record['lead_remark'],
                //     'lfu_date'      => date('Y-m-d', time()),
                //     'lfu_next_date' => $record['lead_next_contact_date'],
                //     'lfu_added_by'  => $profile->user_id,
                //     'lfu_company'   => $profile->user_company
                // ];
                //
                // LeadFollow::insert( $arr );
            } else {
                Lead::where('lead_id', $id)->update($record);
            }

            return redirect("{$company}/leads/")->with('success', "Success! New lead has been created.");
        }

        $customers = $staffs = [];
        if(!empty($profile)) {
            $customers  = User::where('user_is_deleted', 'N')->where('user_role', 'customer')->where('user_company', @$profile->user_company)->get();
            $staffs     = User::where('user_is_deleted', 'N')->where('user_role', 'staff')->where('user_company', @$profile->user_company)->get();
        }

        $visa_types = VisaType::get();

        session()->forget('destinations');

        $data 	    = compact('page', 'title', 'edit', 'customers', 'staffs', 'countries', 'states', 'cities', 'visa_types', 'country_id');
        return view('backend/layout', $data);
    }

    public function follow_up(Request $request, $company, $type = 'today') {
        $profile        = $this->profile( $request );

        $search         = $request->input('search');

        $query          = Lead::with(['user'])->leftJoin('lead_follow_up AS lf' , function($query) {
                           $query->on('leads.lead_id','=','lf.lfu_lid')
                                 ->whereRaw('lfu_id IN (select MAX(lfu.lfu_id) from tts_lead_follow_up AS lfu group by lfu.lfu_lid)');
                        })->where('lead_company', @$profile->user_company);

        if($type == 'today')
            $query   = $query->whereRaw('DATE(`lfu_next_date`) = ?', [ date('Y-m-d', time()) ]);
        // else
        //     $query   = $query->whereRaw('DATE(`lfu_next_date`) != ?', [ date('Y-m-d', time()) ]);

        if(!empty($search['enq_for'])) {
            $query->where('lead_query_for', $search['enq_for']);
        }

        if(!empty($search['keyword'])) {
            $s       = trim( $search['keyword'] );
            $lead_id = substr( $s, 0, 1 ) == '#' ? intval( substr( $s, 1, strlen($s) ) ) : intval( $s );

            $query->where('lead_id', $lead_id)
                  ->orWhere('lfu_message', 'LIKE', '%'.$s.'%');
        }

        $records = $query->paginate(30);

        // dd($records);

        $page            = "lead_follow_ups";
        $title           = "Lead Follow Ups";
        $data 	         = compact('page', 'title', 'records', 'type', 'search');
        return view('backend/layout', $data);
    }

    public function management(Request $request, $company) {
        $search          = [];

        if($request->isMethod('post')) {
            $lead_staff = $request->lead_staff;
            $checked    = $request->check;

            Lead::whereIn('lead_id', $checked)->update( ['lead_staff' => $lead_staff] );

            return redirect()->back()->with('success', 'Selected lead(s) are assigned successfully.');
        }

        $query           = Lead::with(['staff'])->where('lead_is_deleted', 'N');

        $assigned        = $request->assigned;
        if(!empty($assigned) && $assigned == "yes") {
            $query->where('lead_staff', '!=', '0');
        } elseif(!empty($assigned) && $assigned == "no") {
            $query->where('lead_staff', '=', '0');
        }

        $records         = $query->orderBy('lead_id', 'DESC')->paginate(30);

        $staffs          = User::where('user_is_deleted', 'N')->where('user_role', 'staff')->get();
        $page            = "lead_management";
        $title           = "Lead Management ";
        $data 	         = compact('page', 'title', 'records', 'staffs', 'search');
        return view('backend/layout', $data);
    }
}
