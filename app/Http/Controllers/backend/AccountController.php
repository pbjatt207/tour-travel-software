<?php
namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

use App\Model\Role;
use App\Model\User;
use App\Model\Hotel;
use App\Model\Country;
use App\Model\State;
use App\Model\City;
use App\Model\Pincode;
use App\Model\Department;

class AccountController extends BaseController {
    public function index(Request $request, $company, $role, $id = NULL) {
        $profile = $this->profile($request);

        $role   = Role::where('role_slug', $role)->first();
        if(empty($role->role_id)) {
            return redirect( url( $company.'404-error' ) );
        }

        if(!empty($request->id) && !empty($request->action)) {
            // echo $request->action; die;
            $action_value = "";
            switch( $request->action ) {
                case 'enable':
                    User::where('user_id', $request->id)->update( ['user_is_blocked' => 'Y'] );
                    $message = "User is unblocked now.";
                    break;

                case 'disable':
                    User::where('user_id', $request->id)->update( ['user_is_blocked' => 'N'] );
                    $message = "User is blocked now.";
                    break;
            }

            return redirect()->back()->with('success', $message);
        }

        $title 	= $role->role_name." Accounts";
        $page 	= "view_account";

        $states = $cities = $pincodes = array();
        $search = $request->input('search');

        $withAction = ['country', 'state', 'city', 'pincode'];

        if($role->role_slug == "hotel") {
            $withAction[] = "hotel";
        }

        $query   =  User::with($withAction)->where('user_is_deleted', 'N')->where('user_role', $role->role_slug)->where('user_company', $profile->user_company);

        if(!empty($search['keyword'])) {
            $query->where(function($q) use($search, $role) {
                $s = trim( $search['keyword'] );

                $q->where( 'user_name', 'LIKE', '%'.$s.'%' )
                  ->orWhere( 'user_login', 'LIKE', '%'.$s.'%' )
                  ->orWhere( 'user_mobile', 'LIKE', '%'.$s.'%' )
                  ->orWhere( 'user_email', 'LIKE', '%'.$s.'%' )
                  ->orWhere( 'user_address1', 'LIKE', '%'.$s.'%' )
                  ->orWhere( 'user_address2', 'LIKE', '%'.$s.'%' );
            });
        }

        $countries = Country::where('country_is_deleted', 'N')->get();
        if(!empty($search['country'])) {
            $query->where( 'user_country', $search['country'] );

            $states = State::where('state_is_deleted', 'N')->where('state_country', $search['country'])->get();
        }
        if(!empty($search['state'])) {
            $query->where( 'user_state', $search['state'] );

            $cities = City::where('city_is_deleted', 'N')->where('city_state', $search['state'])->get();
        }
        if(!empty($search['city'])) {
            $query->where( 'user_city', $search['city'] );

            $pincodes = Pincode::where('pin_is_deleted', 'N')->where('pin_city', $search['city'])->get();
        }
        if(!empty($search['pincode'])) {
            $query->where( 'user_pincode', $search['pincode'] );
        }


        $records = $query->paginate(30);

        $data 	= compact('page', 'title', 'records', 'role', 'countries', 'states', 'cities', 'pincodes', 'search');
        return view('backend/layout', $data);
    }

    public function add(Request $request, $company, $role, $id = null) {
        $profile = $this->profile($request);
        $role   = Role::where('role_slug', $role)->first();
        if(empty($role->role_id)) {
            return redirect( url( $company.'404-error' ) );
        }

        $edit = $states = $cities = $pincodes = array();
        if(!empty($id)) {
            $withAction = [];

            if($role->role_slug == "hotel") {
                $withAction[] = "hotel";
            }

            $edit       = User::with($withAction)->where('user_id', $id)->first();
        }

        $countries = Country::where('country_is_deleted', 'N')->get();
        if(!empty($edit->user_country)) {
            $states = State::where('state_is_deleted', 'N')->where('state_country', $edit->user_country)->get();
        }
        if(!empty($edit->user_state)) {
            $cities = City::where('city_is_deleted', 'N')->where('city_state', $edit->user_state)->get();
        }
        if(!empty($edit->user_city)) {
            $pincodes = Pincode::where('pin_is_deleted', 'N')->where('pin_city', $edit->user_city)->get();
        }

        $title 	= "Add ".$role->role_name;
        $page 	= "add_account";
        $role_name = ucwords($role);

        if($request->isMethod('post')) {
            $user                 = $request->input('user');
            if($role->role_slug == "hotel") {
                $hotel            = $request->input('hotel');
            }

            $isExistsQuery = User::where('user_login', 'LIKE', $user['user_login'])->where('user_company', $profile->user_company);

            if(!empty($id)) {
                $isExistsQuery->where('user_id','!=',$id);
            }

            $isExists = $isExistsQuery->count();

            if(!$isExists) :
                $user['user_role']          = $role->role_slug;
                $user['user_name']          = trim( $user['user_fname']." " .$user['user_lname'] );
                $user['user_updated_on']    = date('Y-m-d H:i:s', time());
                $user['user_added_by']      = $profile->user_id;
                $user['user_company']       = $profile->user_company;

                if(!empty($user['user_password'])) {
                    $user['user_password']  = password_hash($user['user_password'], PASSWORD_DEFAULT);
                }

                $user = array_filter($user);

                if(empty($id)) {
                    $user['user_created_on'] = date('Y-m-d H:i:s', time());
                    $id = User::insertGetId($user);

                    $message = "Success! New ".$role->role_name." account has been created.";
                } else {
                    User::where('user_id', $id)->update($user);

                    $message = "Success! New ".$role->role_name." account has been updated.";
                }

                if(!empty($hotel)) {
                    if(empty($edit->hotel->hotel_id)) {
                        $hotel['hotel_uid'] = $id;
                        Hotel::insert( $hotel );
                    } else {
                        Hotel::where('hotel_id', $edit->hotel->hotel_id)->update( $hotel );
                    }
                }

                if ($request->hasFile('user_image')) {
                    if(!empty($edit->user_image) && file_exists(public_path()."/imgs/users/{$company}/".$edit->user_image)) {
                        unlink(public_path()."/imgs/users/{$company}/".$edit->user_image);
                    }

                    $image           = $request->file('user_image');
                    $name            = 'IMG'.$id.'.'.$image->getClientOriginalExtension();
                    $destinationPath = "public/imgs/users/{$company}/";
                    $image->move($destinationPath, $name);

                    if(!empty($edit->user_image)) {
                        $name .= "?v=".uniqid();
                    }

                    User::where('user_id', $id)->update( array('user_image' => $name) );
                }

                return redirect("{$company}/account/".$role->role_slug)->with('success', $message);

            else:
                return redirect()->back()->with('danger', "Failed! Login Id already exists, please try another.");
            endif;
        }

        $data 	= compact('page', 'title', 'role_name', 'edit', 'role', 'countries', 'cities', 'states', 'pincodes');
        if(@$role->role_slug == "staff") {
            $data['departments'] = Department::where( 'dept_added_by', $profile->user_id )->get();
        }
        return view('backend/layout', $data);
    }
    public function change_password(Request $request) {
        $title   = "Change Password";
        $page 	 = "change_password";

        $profile = DB::table('users')
            ->where('user_id', session('user_auth'))
            ->first();

        if($request->isMethod('post')) {
            $post = $request->input('record');

            if(password_verify($post['current_password'], $profile->user_password)) {
                $new_password = password_hash($post['new_password'], PASSWORD_DEFAULT);
                User::where('user_id', $profile->user_id)->update( ['user_password' => $new_password] );

                return redirect()->back()->with('success', 'Success! Your password has been changed');
            } else {
                return redirect()->back()->with('danger', "Failed! Current password is not matched.");
            }
        }

        // $records = User::where('user_role', $role)->where('user_is_deleted', 'N')->paginate(10);

        $data 	 = compact('page', 'title');
        return view('backend/layout', $data);
    }
    public function logout($company) {
        session()->forget('user_auth');
        return redirect( url( $company ) )->with('success', 'You\'ve been logged out successfully.');
    }
}
