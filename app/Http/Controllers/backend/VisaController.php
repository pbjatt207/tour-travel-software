<?php
namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

use App\Model\Visa;
use App\Model\Lead;
use App\Model\User;
use App\Model\Role;
use App\Model\Country;
use App\Model\State;
use App\Model\City;

class VisaController extends BaseController {
    public function index(Request $request, $company, $id = NULL) {
        $profile = $this->profile($request);

        $title 	= "Visa";
        $page 	= "view_visa";

        $states = $cities = $pincodes = array();
        $search = $request->input('search');

        $withAction = ['customer'];

        $query   =  Visa::with($withAction)->where('visa_is_deleted', 'N')->where('visa_company', $profile->user_company);

        if(!empty($search['keyword'])) {
            $query->where(function($q) use($search) {
                $s  = trim( $search['keyword'] );
                // echo $s;
                $id = substr($s, 0, 1) == '#' ? intval( substr($s, 1, strlen($s)) ) : intval( $s );
                $q->where('visa_id', '=', $id);
            });
        }

        $records = $query->orderBy('visa_id', 'DESC')->paginate(30);

        $data 	= compact('page', 'title', 'records', 'search');
        return view('backend/layout', $data);
    }

    public function add(Request $request, $company, $id = NULL) {
        $profile = $this->profile($request);

        $country = Country::where('country_is_deleted', 'N')->where('country_name', 'LIKE', 'India')->first();

        $countries = Country::where('country_is_deleted', 'N')->orderBy('country_name')->get();
        $edit = $states = $cities = $pincodes = array();
        if(!empty($id)) {
            $edit       = Visa::find($id);
        }

        if(!empty($country->country_id)) {
            $states = State::where('state_is_deleted', 'N')->where('state_country', $country->country_id)->orderBy('state_name')->get();
        }

        if(!empty($edit->visa_state)) {
            $cities = City::where('city_is_deleted', 'N')->where('city_state', $edit->visa_state)->orderBy('city_name')->get();
        }

        $title 	= empty($edit->visa_id) ? "Visa Application" : "Visa Application";
        $page 	= "add_visa";

        if($request->isMethod('post')) {
            $record = $request->input('record');

            $record['visa_updated_on']    = date('Y-m-d H:i:s', time());
            $record['visa_added_by']      = $profile->user_id;
            $record['visa_company']       = $profile->user_company;

            $record = array_filter($record);

            if(empty($id)) {
                $user['visa_created_on'] = date('Y-m-d H:i:s', time());
                $id = Visa::insertGetId($record);
            } else {
                Visa::where('visa_id', $id)->update($record);
            }

            return redirect("{$company}/visa/")->with('success', "Success! New application has been created.");
        }

        if(!empty($profile)) {
            $customers  = User::where('user_is_deleted', 'N')->where('user_role', 'customer')->where('user_company', @$profile->user_company)->get();
            $staffs     = User::where('user_is_deleted', 'N')->where('user_role', 'staff')->where('user_company', @$profile->user_company)->get();

            $leads      = Lead::where('lead_query_for', 'Visa')->where('lead_is_deleted', 'N')->get();
        }

        $data 	    = compact('page', 'title', 'edit', 'customers', 'staffs', 'countries', 'states', 'cities', 'leads');
        return view('backend/layout', $data);
    }

    public function application(Request $request, $company, $id = NULL) {
        $profile = $this->profile($request);

        $country = Country::where('country_is_deleted', 'N')->where('country_name', 'LIKE', 'India')->first();

        $countries = Country::where('country_is_deleted', 'N')->orderBy('country_name')->get();
        $edit = $states = $cities = $pincodes = array();
        if(!empty($id)) {
            $edit       = Visa::find($id);
        }

        if(!empty($country->country_id)) {
            $states = State::where('state_is_deleted', 'N')->where('state_country', $country->country_id)->orderBy('state_name')->get();
        }

        if(!empty($edit->visa_state)) {
            $cities = City::where('city_is_deleted', 'N')->where('city_state', $edit->visa_state)->orderBy('city_name')->get();
        }

        $title 	= empty($edit->visa_id) ? "Visa Application" : "Visa Application";
        $page 	= "visa_application";

        if($request->isMethod('post')) {
            $record = $request->input('record');

            $record['visa_updated_on']    = date('Y-m-d H:i:s', time());
            $record['visa_added_by']      = $profile->user_id;
            $record['visa_company']       = $profile->user_company;

            $record = array_filter($record);

            if(empty($id)) {
                $user['visa_created_on'] = date('Y-m-d H:i:s', time());
                $id = Visa::insertGetId($record);
            } else {
                Visa::where('visa_id', $id)->update($record);
            }

            return redirect("{$company}/visa/")->with('success', "Success! New application has been created.");
        }

        if(!empty($profile)) {
            $customers  = User::where('user_is_deleted', 'N')->where('user_role', 'customer')->where('user_company', @$profile->user_company)->get();
            $staffs     = User::where('user_is_deleted', 'N')->where('user_role', 'staff')->where('user_company', @$profile->user_company)->get();
        }

        $data 	    = compact('page', 'title', 'edit', 'customers', 'staffs', 'countries', 'states', 'cities');
        return view('backend/layout', $data);
    }

    public function status($company, $id, $status) {
        $date = date('Y-m-d H:i:s', time());
        // echo $id.' - '. $status; die;
        Visa::where('visa_id', $id)->update( ['visa_status' => $status, 'visa_updated_on' => $date] );

        return redirect()->back()->with('success', 'Visa status has been changed.');
    }
}
