<?php
namespace App\Http\Controllers\backend;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use DB;

use App\Model\User;
use App\Model\LeadFollow;

class Ajax extends BaseController {
    public function index( Request $request, $company, $action = NULL ) {
    	$post   = $request->input();

        $param  = compact('post');
        $re     = call_user_func_array(array($this, $action), $param);

        return response()->json($re);
    }

    public function upload_image( Request $request ) {

        if ($request->hasFile('file')) {
            $image = $request->file('file');
            $name = uniqid().'.'.$image->getClientOriginalExtension();
            $destinationPath = 'public/uploads';
            $image->move($destinationPath, $name);

            $re = array(
                'status'    => TRUE,
                'location' => url('/').'/'.$destinationPath.'/'.$name
            );
        } else {
            $re = array(
                'status'    => FALSE,
                'message'   => 'Error'
            );
        }

        return response()->json($re);
    }

    public function user_login( Request $request ) {

        $post = $request->input('record');
        $is_exists = DB::table('users')
                        ->select( DB::raw('COUNT(*) AS total, user_id, user_password') )
                        ->where('user_login', $post['user_login'])
                        ->where('user_company', $post['user_company'])
                        ->where('user_is_deleted', 'N')
                        ->first();

        if($is_exists->total == 1) {
            if(password_verify($post['user_password'], $is_exists->user_password)) {
                session(['user_auth' => $is_exists->user_id]);

                $re = array(
                    'status'    => TRUE,
                    'message'   => 'Login success! Redirecting, please wait...'
                );
            } else {
                $re = array(
                    'status'    => FALSE,
                    'message'   => 'Login failed! Passowrd is not matched.'
                );
            }
        } else {
            $re = array(
                'status'    => FALSE,
                'message'   => 'Login failed! Username doesn\'t exists.'
            );
        }

        return response()->json($re);
    }

    public function add_customer( $post ) {
        $profile = User::find( session('user_auth') );

        $post                     = $post['record'];

        $post['user_login']       = $post['user_mobile'];
        $post['user_role']        = 'customer';
        $post['user_company']     = $profile->user_company;
        $post['user_added_by']    = $profile->user_id;
        $post['user_created_on']  = date('Y-m-d H:i:s', time());
        $post['user_updated_on']  = date('Y-m-d H:i:s', time());

        $isExists = User::where('user_login', 'LIKE', $post['user_login'])->where('user_company', $profile->user_company)->count();

        if(!$isExists) {
            $user_id = User::insertGetId( $post );

            $re = [
                'status'      => TRUE,
                'user_id'     => $user_id,
                'user_name'   => $post['user_name'],
                'user_mobile' => $post['user_mobile']
            ];
        } else {
            $re = [
                'status'    => FALSE,
                'message'   => 'Moble no. already exists, please try with another mobile no.'
            ];
        }

        return $re;
    }

    public function follow_up( $post ) {
        $lead_id    = $post['id'];
        $records    = LeadFollow::with(['lead'])->where('lfu_lid', $lead_id )->orderBy('lfu_id', 'DESC')->get();
        $html       = view('backend.template.follow_up', compact('records', 'lead_id'))->render();

        $re   = [
            'html'  => $html
        ];

        return $re;
    }

    public function get_account( $post ) {
        $data = User::find( $post['id'] );

        $re = [
            'status'    => TRUE,
            'data'      => $data
        ];

        return $re;
    }

    public function get_states($post) {
        $states = DB::table('states')->where('state_is_deleted', 'N')->where('state_country', $post['id'])->get();

        $re = array(
            'data'  => $states
        );

        return $re;
    }

    public function get_cities($post) {
        $cities = DB::table('cities')->where('city_is_deleted', 'N')->where('city_state', $post['id'])->get();

        $re = array(
            'data'  => $cities
        );

        return $re;
    }

    public function get_all_cities( $post ) {
        $cities = DB::table('cities')->where('city_is_deleted', 'N')->select('city_id', 'city_name', 'city_short_name')->get()->toArray();

        $cityArr = [];
        foreach($cities as $city) {
            $cityArr[] = [
                "label" => $city->city_name,
                "value" => $city->city_name
            ];
        }

        return $cityArr;
    }

    public function get_pincodes($post) {
        $pincodes = DB::table('pincodes')->where('pin_is_deleted', 'N')->where('pin_city', $post['id'])->get();

        $re = array(
            'data'  => $pincodes
        );

        return $re;
    }

    public function save_destination_sess( $post ) {
        $session = session('destinations');

        $current = !empty($current) ? $current : [];

        $session[] = $post['city'];

        session( ['destinations' => $session] );

        $html = (string) \View::make('backend.template.view_destinations', ['session' => $session]);
        print_r($html);
        $re = [
            'status' => TRUE,
            'html'   => $html
        ];

        return $re;
    }
}
