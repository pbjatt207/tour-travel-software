<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Model\Department;

class DepartmentController extends BaseController {
    public function index(Request $request, $company, $id = null) {
        $profile = $this->profile($request);

    	$edit = [];
    	if(!empty($id)) {
    		$edit = Department::find($id);
    	}

    	if($request->isMethod('post')) {
    		$input = $request->input('record');
    		if(!empty($input)) {

                $isExistsQuery = Department::where('dept_name', 'LIKE', $input['dept_name'])->where('dept_added_by', $profile->user_id);

                if(!empty($edit->dept_id)) {
                    $isExistsQuery->where('dept_id', '!=', $edit->dept_id);
                }

                $isExists = $isExistsQuery->count();

                if(!$isExists) :
                    $input['dept_added_by'] = $profile->user_id;

    	    		if(empty($edit->dept_id)) {
    		    		$id   = Department::insertGetId($input);
                        $mess = "A new record has been added.";
    		    	} else {
    		    		Department::where('dept_id', $id)->update($input);
                        $mess = "A record has been updated.";
    		    	}
                else :
                    return redirect()->back()->with('danger', 'Department name already exists.');
                endif;
		    }

		    $check = $request->input('check');
		    if(!empty($check)) {
		    	Department::whereIn('dept_id', $check)->delete();
                $mess = "Selected record(s) has been removed.";
		    }

	    	return redirect()->back()->with('success', $mess);
    	}
        // \DB::enableQueryLog();
        $query   = Department::where('dept_added_by', $profile->user_id);
        $search  = $request->input('search');
        if(!empty($search['keyword'])) {
            $query->whereRaw("LOWER(dept_name) LIKE '%".trim(strtolower($search['keyword']))."%'");
        }
    	$records = $query->orderBy('dept_name', 'ASC')->paginate(30);
        // dd(\DB::getQueryLog());

        $title 	= "Department";
        $page 	= "department";
        $data 	= compact('page', 'title', 'records', 'edit', 'search');
        return view('backend/layout', $data);
    }
}
