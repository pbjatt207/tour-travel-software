<?php
namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Model\User;
use App\Model\Company;

class BaseController extends Controller {
    public function profile(Request $request) {
        $userId  = session('user_auth');
        $profile = User::with(['company'])->find($userId);

        return $profile;
    }
}
