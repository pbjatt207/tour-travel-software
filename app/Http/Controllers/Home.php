<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;

class Home extends BaseController {
    public function index() {
    	$page = "homepage";
    	$data = compact('page');
    	return view('frontend/layout', $data);
    }
}
