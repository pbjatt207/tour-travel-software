<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class Query extends Model {
    public function create_slug($title = "", $table_name, $field_name, $idField, $id) {
		$slug 		= preg_replace("/-$/","",preg_replace('/[^a-z0-9]+/i', "-", strtolower($title)));

		$row 		= DB::table($table_name)->select( [DB::raw('COUNT(*) as NumHits')] )->where($field_name, 'LIKE', "$slug%")->where($idField,'!=',$id)->first();
		$numHits 	= $row->NumHits;

		return ($numHits > 0) ? ($slug . '-' . $numHits) : $slug;
	}
}
